<?php

namespace JOYAS\JoyasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsuarioType extends AbstractType {

    //Atributos
    protected $perfil;
    protected $perfiles;

    //Constructor
    public function __construct($perfil) {
        $this->perfil = $perfil;
        if ($perfil == 'ADMINISTRADOR') {
            $this->perfiles = array('ADMINISTRADOR' => 'ADMINISTRADOR',
                'ADMIN' => 'ADMIN',
                'OPERADOR' => 'OPERADOR');
        } else if ($perfil == 'ADMIN') {
            $this->perfiles = array(
                'ADMIN' => 'ADMIN',
                'OPERADOR' => 'OPERADOR');
        }
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('login', 'text', array(
                    'attr' => array('pattern' => '|^[a-zA-Z0-9]*$|',
                        'title' => 'Solo letras y numeros. Sin espacios ni caracteres especiales.')
                ))
                ->add('clave', 'text', array(
                    'label' => 'Clave',
                    'attr' => array(
                        'class' => 'form-control')
                ))
                ->add('perfil', 'choice', array(
                    'choices' => $this->perfiles,
                    'attr' => array(
                        'class' => 'form-control'),
                    'required' => true
                ))
                ->add('mail', 'email', array(
                    'label' => 'E-mail',
                    'attr' => array(
                        'class' => 'form-control')
                ))
                ->add('unidadNegocio', 'entity', array(
                    'label' => 'Unidad de Negocio',
                    'required' => false,
                    'class' => 'JOYASJoyasBundle:UnidadNegocio',
                    'query_builder' => function (\JOYAS\JoyasBundle\Entity\UnidadNegocioRepository $repository) {
                        return $repository->createQueryBuilder('u')->where('u.estado = ?2')->setParameter(2, 'A')->orderBy('u.descripcion', 'ASC');
                    }))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'JOYAS\JoyasBundle\Entity\Usuario'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'joyas_joyasbundle_usuario';
    }

}
