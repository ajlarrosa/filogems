<?php

namespace JOYAS\JoyasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CategoriasubcategoriaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('categoria', 'entity', array(
                    'class' => 'JOYASJoyasBundle:Categoria',
                    'label' => 'Categoría',
                    'query_builder' => function (\JOYAS\JoyasBundle\Entity\CategoriaRepository $repository) {
                        return $repository->createQueryBuilder('u')->where('u.estado = ?1')->setParameter(1, 'A')->orderBy('u.descripcion');
                    }
                ))
                ->add('subcategoria', 'entity', array(
                    'class' => 'JOYASJoyasBundle:Subcategoria',
                    'label' => 'Subcategoría',
                    'query_builder' => function (\JOYAS\JoyasBundle\Entity\SubcategoriaRepository $repository) {
                        return $repository->createQueryBuilder('u')->where('u.estado = ?1')->setParameter(1, 'A')->orderBy('u.descripcion');
                    }
                ))
                ->add('metal', 'entity', array(
                    'class' => 'JOYASJoyasBundle:Metal',
                    'label' => 'Metal',
                    'empty_data' => null,
                    'empty_value' => 'Seleccione',
                    'data' => 'Seleccione',
                    'query_builder' => function (\JOYAS\JoyasBundle\Entity\MetalRepository $repository) {
                        return $repository->createQueryBuilder('u')->where('u.estado = ?1')->setParameter(1, 'A')->orderBy('u.descripcion');
                    }
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'JOYAS\JoyasBundle\Entity\Categoriasubcategoria'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'joyas_joyasbundle_categoriasubcategoria';
    }

}
