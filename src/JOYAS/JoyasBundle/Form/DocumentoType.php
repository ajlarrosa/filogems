<?php

namespace JOYAS\JoyasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DocumentoType extends AbstractType {

    private $gasto;

    public function __construct($tipo) {

        if ($tipo == 'G') {
            $this->gasto = '1';
        } else {
            $this->gasto = '2';
        }
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('fecha')
                ->add('moneda', 'choice', array(
                    'attr' => array('class' => 'form-control'),
                    'choices' => array(
                        1 => 'ARG',
                        2 => 'USD'
            )))
                ->add('tipoGasto')
                ->add('importe', 'integer', array('label' => 'Importe',
                    'attr' => array('step' => 0.01,                       
                        'class' => 'form-control'),
                    'required' => false))
                ->add('oro', 'integer', array('label' => 'Oro',
                    'attr' => array('step' => 0.01,
                        'min' => 0,
                        'class' => 'form-control'),
                    'required' => false))
                ->add('plata', 'integer', array('label' => 'Plata',
                    'attr' => array('step' => 0.01,
                        'min' => 0,
                        'class' => 'form-control'),
                    'required' => false))
                ->add('tipoDocumento')
                ->add('descripcion')
                ->add('nrofactura', 'text', array(
                    'label' => 'Nro. de Factura',
                    'attr' => array(
                        'class' => 'form-control'),
                    'required' => false
                ))
                ->add('nroremito', 'text', array(
                    'label' => 'Nro. de Remito',
                    'attr' => array(
                        'class' => 'form-control'),
                    'required' => false
                ))
                ->add('observacion', 'textarea', array('label' => 'Observaciones',
                    'required' => false,
                    'attr' => array('class' => 'form-control',
                        'style' => 'height:200px')))
                ->add('nrocomprobante', 'text', array(
                    'label' => 'Nro. de Comprobante',
                    'attr' => array(
                        'class' => 'form-control'),
                    'required' => false
                ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'JOYAS\JoyasBundle\Entity\Documento'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'joyas_joyasbundle_documento';
    }

}
