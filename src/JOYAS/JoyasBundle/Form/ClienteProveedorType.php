<?php

namespace JOYAS\JoyasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class ClienteProveedorType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('razonSocial', 'text', array('label' => $options['razonSocialLabel']))
                ->add('provincia', 'entity', array(
                    'class' => 'JOYASJoyasBundle:Provincia',
                    'label' => 'Provincia',
                    'mapped' => false,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                                ->where('c.activo = true')
                                ->addOrderBy('c.descripcion', 'ASC');
                    }))
                ->add('localidad', 'entity', array(
                    'class' => 'JOYASJoyasBundle:Localidad',
                    'label' => 'Localidad',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                                ->where('c.activo = true')
                                ->addOrderBy('c.descripcion', 'ASC');
                    }))
                ->add('direccion', 'text', array('label' => $options['direccionLabel']))
                ->add('telefono', 'text', array('label' => $options['telefonoLabel']))
                ->add('celular')
                ->add('mail')
                ->add('cuit')
                ->add('clienteProveedor', 'choice', array(
                    'label' => ' Cliente o Proveedor',
                    'attr' => array('class' => 'form-control'),
                    'choices' => array(
                        1 => 'Cliente',
                        2 => 'Proveedor'
            )))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'JOYAS\JoyasBundle\Entity\ClienteProveedor',
            'razonSocialLabel' => 'Razón Social',
            'direccionLabel' => 'Dirección',
            'telefonoLabel' => 'Teléfono'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'joyas_joyasbundle_clienteproveedor';
    }

}
