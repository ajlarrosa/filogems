<?php

namespace JOYAS\JoyasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProvinciaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('descripcion', 'text', array(
                    'label' => 'Provincia',
                    'attr' => array(
                        'class' => 'form-control'),
                    'required' => true
                ))
                ->add('activo', 'checkbox', array(
                    'label' => 'La Provincia se encuentra activa?',
                    'attr' => array(
                        'class' => 'checkbox-inline'),
                    'required' => false
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'JOYAS\JoyasBundle\Entity\Provincia'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'ale_arbundle_provincia';
    }

}
