<?php

namespace JOYAS\JoyasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class ChequeType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('fechaemision', 'text', array('label' => 'Fecha de Emisión',
                    'attr' => array(
                        'class' => 'form-control datetimepicker '),
                    'required' => false))
                ->add('fechacobro', 'text', array('label' => 'Fecha de Cobro',
                    'attr' => array(
                        'class' => 'form-control datetimepicker '),
                    'required' => false))
                ->add('importe', 'integer', array('label' => 'Importe',
                    'attr' => array(
                        'class' => 'form-control',
                        'step' => 0.01,
                        'min' => 0),
                    'required' => true))
                ->add('nrocheque', 'text', array('label' => 'Nro. Cheque',
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'required' => false))
                ->add('firmantes', 'text', array('label' => 'Firmantes',
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'required' => false))
                ->add('cuit', 'text', array('label' => 'CUIT',
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'required' => false))
                //->add('documento')
                ->add('banco', 'entity', array(
                    'class' => 'JOYASJoyasBundle:Banco',
                    'label' => 'Nombre del Banco',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                                ->addOrderBy('c.descripcion', 'ASC');
                    },
                    'required' => false
                ))
                ->add('tipocheque', 'entity', array(
                    'class' => 'JOYASJoyasBundle:TipoCheque',
                    'label' => 'Tipo de Cheque',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                                ->addOrderBy('c.descripcion', 'ASC');
                    },
                    'required' => false
                ))
                //->add('pago')
                //->add('estado')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'JOYAS\JoyasBundle\Entity\Cheque'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'joyas_joyasbundle_cheque';
    }

}
