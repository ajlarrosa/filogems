<?php

namespace JOYAS\JoyasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LocalidadType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('provincia', 'entity', array(
                    'class' => 'JOYASJoyasBundle:Provincia',
                    'label' => 'Provincia',
                    'attr' => array(
                        'class' => 'form-control'),
                    'required' => true
                ))
                ->add('descripcion', 'text', array(
                    'label' => 'Localidad',
                    'attr' => array(
                        'class' => 'form-control'),
                    'required' => true
                ))
                 ->add('activo', 'checkbox', array(
                    'label' => 'La Provincia se encuentra activa?',
                    'attr' => array(
                        'class' => 'checkbox-inline'),
                    'required' => false
                ))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'JOYAS\JoyasBundle\Entity\Localidad'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'ale_arbundle_localidad';
    }

}
