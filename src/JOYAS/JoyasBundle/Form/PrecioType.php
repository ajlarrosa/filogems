<?php

namespace JOYAS\JoyasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\HttpFoundation\Request;

class PrecioType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    private $value;

    public function __construct($idUnidad) {
        $this->value = $idUnidad;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        if ($this->value != '') {
            $builder
                    ->add('listaPrecio', 'entity', array(
                        'class' => 'JOYASJoyasBundle:ListaPrecio',
                        'label' => 'Lista de precios',
                        'query_builder' => function (\JOYAS\JoyasBundle\Entity\ListaPrecioRepository $repository) {
                            return $repository->createQueryBuilder('u')->where('u.unidadNegocio = ?1 and u.estado= ?2')->setParameter(1, $this->value)->setParameter(2, 'A')->orderBy('u.descripcion','ASC');
                        }
                    ))
                    ->add('producto', 'entity', array(
                        'class' => 'JOYASJoyasBundle:Producto',
                        'label' => 'Producto',
                        'query_builder' => function (\JOYAS\JoyasBundle\Entity\ProductoRepository $repository) {
                            return $repository->createQueryBuilder('u')->where('u.unidadNegocio = ?1 and u.estado= ?2')->setParameter(1, $this->value)->setParameter(2, 'A')->orderBy('u.codigo','ASC');
                        }
            ));
        } else {
            $builder
                    ->add('listaPrecio', 'entity', array(
                        'class' => 'JOYASJoyasBundle:ListaPrecio',
                        'label' => 'Lista de precios',
                        'query_builder' => function (\JOYAS\JoyasBundle\Entity\ListaPrecioRepository $repository) {
                            return $repository->createQueryBuilder('u')->where('u.estado = ?1')->setParameter(1, 'A')->orderBy('u.descripcion','ASC');
                        }))
                    ->add('producto', 'entity', array(
                        'class' => 'JOYASJoyasBundle:Producto',
                        'label' => 'Producto'));
        }
        $builder
                ->add('valor', 'text', array('label' => 'Precio'))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'JOYAS\JoyasBundle\Entity\Precio'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'joyas_joyasbundle_precio';
    }

}
