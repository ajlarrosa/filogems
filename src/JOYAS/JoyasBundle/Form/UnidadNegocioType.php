<?php

namespace JOYAS\JoyasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UnidadNegocioType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('descripcion', 'text', array('label' => $options['descripcionLabel']))
                ->add('responsable')
                ->add('direccion', 'text', array('label' => $options['direccionLabel']))
                ->add('telefono', 'text', array('label' => $options['telefonoLabel']))
                ->add('celular')
                ->add('mail')
                 ->add('usaParametrica', 'checkbox', array(
                    'label' => 'Usa paramétricas del ADMINISTRADOR?',
                    'attr' => array(
                        'class' => 'checkbox-inline'),
                    'required' => true
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'JOYAS\JoyasBundle\Entity\UnidadNegocio',
            'descripcionLabel' => 'Descripción',
            'direccionLabel' => 'Dirección',
            'telefonoLabel' => 'Teléfono'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'joyas_joyasbundle_unidadnegocio';
    }

}
