<?php

namespace JOYAS\JoyasBundle\Services;

use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;

class MailsManager {

    /**
     * 
     * @var Container
     */
    public $container;

    /**
     * @var EntityManager
     */
    public $em;

    public function __construct(Container $container) {
        $this->container = $container;
        $this->em = $container->get('doctrine.orm.entity_manager');
    }

    public function enviarMail($datos) {
        try {
            if (strpos($_SERVER['SERVER_NAME'], $this->container->getParameter('servidor_productivo')) !== false) {
                $sendTo = $this->container->getParameter('mail_user');
            } else {
                $sendTo = $this->container->getParameter('mail_test');
            }
            $envioMails = $this->container->getParameter('envio_mails');
            echo   $this->container->get('kernel')->getRootDir() . "/../web/bundles/" . $this->container->getParameter('web_bundle') . "/images/logopeq.png";
            $message = \Swift_Message::newInstance()
                    ->setSubject($datos->titulo)
                    ->setFrom([$envioMails => $this->container->getParameter('nombre_cliente')])
                    ->setTo($sendTo);
            $imagen = $message->embed(
                    \Swift_Image::fromPath(
                            realpath(
                                    $this->container->get('kernel')->getRootDir() . "/../web/bundles/" . $this->container->getParameter('web_bundle') . "/images/logo.jpg"
                            )
            ));

            $message->setBody(
                    $this->container->get('templating')->render('JOYASJoyasBundle:Web:mail.html.twig', array(
                        'imagen' => $imagen,
                        'datos' => $datos
                            )
                    ), 'text/html'
            );
            $this->container->get('mailer')->send($message);
            return true;
        } catch (\Swift_TransportException $e) {
            return false;
        }
    }

}
