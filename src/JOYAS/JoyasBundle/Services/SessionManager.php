<?php

namespace JOYAS\JoyasBundle\Services;

use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;

class SessionManager {

    /**
     * 
     * @var Container
     */
    public $container;

    /**
     * @var EntityManager
     */
    public $em;

    /**
     * @var Session
     */
    public $session;

    public function __construct(Container $container) {
        $this->container = $container;
        $this->em = $container->get('doctrine.orm.entity_manager');
        $this->session = $container->get('session');
        $categorias = $this->em->getRepository('JOYASJoyasBundle:Categoria')->findAll();
        $this->setSession('categorias', $categorias);
        if ($this->container->getParameter('nombre_cliente') == 'WillyJhons') {
            $metales = $this->em->getRepository('JOYASJoyasBundle:Metal')->getAllSinonimo();
            $this->setSession('metales', $metales);
        }
    }

    public function startSession() {
        $this->session->start();
        $this->setSession('modulo', '');
        $this->setSession('perfil', '');
        $this->setSession('login', 'false');
        $this->setSession('usuario', '');
        $this->setSession('idUsuario', '');
        $this->setSession('unidad', '');
        $this->setSession('idCliProv', '');
    }

    public function clearSession() {
        $this->session->clear();
        $this->session->getFlashBag()->clear();
    }

    public function closeSession() {
        $this->clearSession();
        $this->startSession();
    }

    public function login($login, $pass) {
        $usuario = $this->em->getRepository('JOYASJoyasBundle:Usuario')->findOneBy(array('login' => $login));

        if ($usuario != null) {
            //get password
            /* 			list($user, $password) = split("///", $usuario->getClave());
              $psw = $this->decryptIt('iSjs');

              $this->setSession('passDec', $psw);
              $this->setSession('pass', $pass);
             */
            if ($usuario->getClave() == $pass) {
                $this->setSession('modulo', '');
                $this->setSession('login', 'true');
                $this->setSession('perfil', $usuario->getPerfil());
                if ($usuario->getPerfil() != 'ADMINISTRADOR') {
                    $this->setSession('unidad', $usuario->getUnidadNegocio()->getId());
                }
                $this->setSession('idUsuario', $usuario->getId());
                $this->setSession('usuario', $login);
                return true;
            }
            $this->addFlash('msgError', 'La contraseña es incorrecta, por favor inténtelo de nuevo.');
            return false;
        }

        $this->addFlash('msgError', 'El usuario no existe.');
        return false;
    }

    /**
     * Agrega un mensaje de tipo msgType a la siguiente response.
     * msgType validos: msgOk, msgInfo, msgWarn, msgError.
     * @param string $msgType
     * @param string $msg
     */
    public function addFlash($msgType, $msg) {
        $this->session->getFlashBag()->add($msgType, $msg);
    }

    /**
     * Setea un parametro en la sesion
     * @param string $attr
     * @param string $value
     * @return mixed
     */
    public function setSession($attr, $value) {
        $this->session->set($attr, $value);
    }

    /**
     * Devuelve un valor de la sesion
     * @param string $attr
     * @return mixed
     */
    public function getSession($attr) {
        return $this->session->get($attr);
    }

    public function isLogged() {
        if ($this->getSession('login') == 'true') {
            return true;
        } else {
            return false;
        }
    }

    public function decryptIt($q) {
        $cryptKey = 'qJB0rGtIn5UB1xG03efyCp';
        $qDecoded = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), base64_decode($q), MCRYPT_MODE_CBC, md5(md5($cryptKey))), "\0");
        return( $qDecoded );
    }

}

function encryptIt($q) {
    $cryptKey = 'qJB0rGtIn5UB1xG03efyCp';
    $qEncoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), $q, MCRYPT_MODE_CBC, md5(md5($cryptKey))));
    return( $qEncoded );
}
