<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\ArrayCollection;
use JOYAS\JoyasBundle\Entity\TipoCheque;
use JOYAS\JoyasBundle\Form\TipoChequeType;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * TipoCheque controller.
 *
 */
class TipoChequeController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionSvc;

    /**
     * Lists all TipoCheque entities.
     *
     */
    public function indexAction() {
        if ($this->sessionSvc->isLogged()) {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JOYASJoyasBundle:TipoCheque')->findAll();

        return $this->render('JOYASJoyasBundle:TipoCheque:index.html.twig', array(
                    'entities' => $entities,
        ));
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
    }
    }

    /**
     * Creates a new TipoCheque entity.
     *
     */
    public function createAction(Request $request) {
        if ($this->sessionSvc->isLogged()) {
        $entity = new TipoCheque();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
                $this->sessionSvc->addFlash('msgOk', 'Tipo de cheque creado exitosamente!');
                return $this->redirect($this->generateUrl('tipocheque', array('id' => $entity->getId())));
        }

        return $this->render('JOYASJoyasBundle:TipoCheque:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
        } else {
            return $this->redirect($this->generateUrl('alerodriguez_homepage'));
    }
    }

    /**
     * Creates a form to create a TipoCheque entity.
     *
     * @param TipoCheque $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TipoCheque $entity) {
        $form = $this->createForm(new TipoChequeType(), $entity, array(
            'action' => $this->generateUrl('tipocheque_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn middle-first')));

        return $form;
    }

    /**
     * Displays a form to create a new TipoCheque entity.
     *
     */
    public function newAction() {
        if ($this->sessionSvc->isLogged()) {
        $entity = new TipoCheque();
        $form = $this->createCreateForm($entity);

        return $this->render('JOYASJoyasBundle:TipoCheque:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
    }
    }

    /**
     * Finds and displays a TipoCheque entity.
     *
     */
    public function showAction($id) {
        if ($this->sessionSvc->isLogged()) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:TipoCheque')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoCheque entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:TipoCheque:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
    }
    }

    /**
     * Displays a form to edit an existing TipoCheque entity.
     *
     */
    public function editAction($id) {
        if ($this->sessionSvc->isLogged()) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:TipoCheque')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoCheque entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:TipoCheque:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
    }
    }

    /**
     * Creates a form to edit a TipoCheque entity.
     *
     * @param TipoCheque $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(TipoCheque $entity) {
        $form = $this->createForm(new TipoChequeType(), $entity, array(
            'action' => $this->generateUrl('tipocheque_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));
        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn middle-first')));
        return $form;
    }

    /**
     * Edits an existing TipoCheque entity.
     *
     */
    public function updateAction(Request $request, $id) {
        if ($this->sessionSvc->isLogged()) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:TipoCheque')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoCheque entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
                $this->sessionSvc->addFlash('msgOk', 'Edición realizada correctamente!');
            return $this->redirect($this->generateUrl('tipocheque_edit', array('id' => $id)));
        }

        return $this->render('JOYASJoyasBundle:TipoCheque:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
    }
    }

    /**
     * Deletes a TipoCheque entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JOYASJoyasBundle:TipoCheque')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TipoCheque entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tipocheque'));
    }

    /**
     * Creates a form to delete a TipoCheque entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('tipocheque_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
