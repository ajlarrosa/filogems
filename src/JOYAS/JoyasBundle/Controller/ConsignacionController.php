<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JOYAS\JoyasBundle\Entity\Consignacion;
use JOYAS\JoyasBundle\Entity\ProductoConsignacion;
use JOYAS\JoyasBundle\Form\ConsignacionType;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Consignacion controller.
 *
 */
class ConsignacionController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionSvc;

    /**
     * Lists all Consignacion entities.
     *
     */
    public function indexAction() {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $em = $this->getDoctrine()->getManager();

        $this->sessionSvc->setSession('listaprecio', '');

        if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
            $entities = $em->getRepository('JOYASJoyasBundle:Consignacion')->getAllActivas($this->sessionSvc->getSession('unidad'));
            $clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllCli($this->sessionSvc->getSession('unidad'));
            $listaPrecios = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->getAllActivas($this->sessionSvc->getSession('unidad'));
        } else {
            $entities = $em->getRepository('JOYASJoyasBundle:Consignacion')->getAllActivas();
            $clientesProveedores = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllCli();
            $listaPrecios = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->getAllActivas();
        }

        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();

        return $this->render('JOYASJoyasBundle:Consignacion:index.html.twig', array(
                    'entities' => $entities,
                    'unidades' => $unidades,
                    'clientesProveedores' => $clientesProveedores,
                    'listaPrecios' => $listaPrecios
        ));
    }

    /**
     * Creates a new Consignacion entity.
     *
     */
    public function createAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entity = new Consignacion();

        $cliPro = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->find($request->get('cliente'));
        $listaPrecio = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->find($request->get('listaPrecio'));

        $entity->setUnidadNegocio($listaPrecio->getUnidadNegocio());
        $descuento = $request->get('descuento');
        $bonificacion = $request->get('bonificacion');
        if (is_numeric($descuento)) {
            $entity->setDescuento($descuento);
        }
        if (is_numeric($descuento)) {
            $entity->setBonificacion($bonificacion);
        }

        $entity->setFecha(new \DateTime());
        if ($request->get('fechavencimiento') != '') {
            $entity->setFechaVencimiento(new \DateTime($request->get('fechavencimiento')));
        }
        $entity->setListaPrecio($listaPrecio);
        $entity->setClienteProveedor($cliPro);
        $entity->setImporte($request->get('importeFinal'));

        $em->persist($entity);
        $em->flush();

        $contador = $request->get('contador');

        for ($x = 0; $x <= $contador; $x++) {
            $codigo = $request->get('codigo' . $x);
            $desc = $request->get('descripcion' . $x);

            if (!is_null($codigo) and $codigo != '' and $desc != '' and ! is_null($desc)) {
                $productoConsignacion = new ProductoConsignacion();

                if ($codigo != '000') {
                    $producto = $em->getRepository('JOYASJoyasBundle:Producto')->findOneBy(array('codigo' => $codigo, 'unidadNegocio' => $entity->getListaPrecio()->getUnidadNegocio()));
                } else {
                    $producto = $em->getRepository('JOYASJoyasBundle:Producto')->findOneBy(array('codigo' => $codigo));
                }

                if (!is_null($producto) and $codigo != '000') {
                    $stock = $producto->getStock() - $request->get('cantidad' . $x);
                    if ($stock < 0) {
                        $stock = 0;
                    }
                    $producto->setStock($stock);
                }

                if (!is_null($producto)) {
                    $productoConsignacion->setProducto($producto);
                    $productoConsignacion->setConsignacion($entity);
                    $productoConsignacion->setPrecio($request->get('precio' . $x));
                    $productoConsignacion->setCantidad($request->get('cantidad' . $x));
                    $entity->addProductosConsignacion($productoConsignacion);
                }
            }
        }

        if (count($entity->getProductosConsignacion()) < 1) {
            $em->remove($entity);
            $em->flush();
            $this->sessionSvc->addFlash('msgError', 'La consignacion debe tener al menos un producto.');
            $this->sessionSvc->setSession('listaprecio', $listaPrecio->getId());
            return $this->redirect($this->generateUrl('consignacion_new'));
        }
        $em->flush();

        $this->sessionSvc->setSession('listaprecio', '');
        $this->sessionSvc->addFlash('msgOk', 'Consignacion registrada.');

        return $this->redirect($this->generateUrl('consignacion_show', array('id' => $entity->getId())));
    }

    /**
     * Creates a form to create a Consignacion entity.
     *
     * @param Consignacion $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Consignacion $entity) {
        $form = $this->createForm(new ConsignacionType(), $entity, array(
            'action' => $this->generateUrl('consignacion_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Consignacion entity.
     *
     */
    public function newAction(Request $request) {
        if ($this->sessionSvc->isLogged()) {
            if ($this->sessionSvc->getSession('listaprecio') == '') {
                $this->sessionSvc->setSession('listaprecio', $request->get('listaprecio'));
            } else {
                $set = $request->get('listaprecio');
                if ($set != '' and isset($set) and $this->sessionSvc->getSession('listaprecio') != $request->get('listaprecio')) {
                    $this->sessionSvc->setSession('listaprecio', $request->get('listaprecio'));
                }
            }

            $entity = new Consignacion();
            $em = $this->getDoctrine()->getManager();

            $listaPrecio = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->find($this->sessionSvc->getSession('listaprecio'));
            $clientes = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->getAllActivas($listaPrecio->getUnidadNegocio());

            return $this->render('JOYASJoyasBundle:Consignacion:new.html.twig', array(
                        'entity' => $entity,
                        'clientes' => $clientes,
                        'listaPrecio' => $listaPrecio,
            ));
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
    }

    /**
     * Finds and displays a Consignacion entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Consignacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Consignacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Consignacion:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Consignacion entity.
     *
     */
    public function editAction($id) {
        if ($this->sessionSvc->isLogged()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JOYASJoyasBundle:Consignacion')->find($id);

            return $this->render('JOYASJoyasBundle:Consignacion:edit.html.twig', array(
                        'entity' => $entity,
                        'algo' => 'editar',
            ));
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
    }

    public function facturarAction($id) {
        if ($this->sessionSvc->isLogged()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JOYASJoyasBundle:Consignacion')->find($id);

            return $this->render('JOYASJoyasBundle:Consignacion:edit.html.twig', array(
                        'entity' => $entity,
                        'algo' => 'facturar',
            ));
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
    }

    public function cerrarAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Consignacion')->find($id);
        $entity->setEstado('I');
        $cant = 0;
        $stock = 0;


        foreach ($entity->getProductosConsignacion() as $prodCon) {
            $stock = $prodCon->getProducto()->getStock();
            $cant = $stock + $prodCon->getCantidad();
            $prodCon->getProducto()->setStock($cant);
        }

        $em->flush();
        $this->sessionSvc->addFlash('msgOk', 'La consignacion ' . $entity->getId() . ' fue cerrada.');
        return $this->redirect($this->generateUrl('consignacion'));
    }

    /**
     * Creates a form to edit a Consignacion entity.
     *
     * @param Consignacion $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Consignacion $entity) {
        $form = $this->createForm(new ConsignacionType(), $entity, array(
            'action' => $this->generateUrl('consignacion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Consignacion entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Consignacion')->find($id);

        
        $descuento = $request->get('descuento');
        $bonificacion = $request->get('bonificacion');
        if (is_numeric($descuento)) {
            $entity->setDescuento($descuento);
        }
        if (is_numeric($descuento)) {
            $entity->setBonificacion($bonificacion);
        }
      
        if ($request->get('fechavencimiento') != '') {
            $entity->setFechaVencimiento(new \DateTime($request->get('fechavencimiento')));
        }
    
        $entity->setImporte($request->get('importeFinal'));

        $em->flush();

        $contador = $request->get('contador');

        $cantVieja = count($entity->getProductosConsignacion());

        for ($x = 0; $x <= $contador; $x++) {
            if ($x < $cantVieja) {
                $codigo = $request->get('codigo' . $x);
                $cantidad = $request->get('cantidad' . $x);
                foreach ($entity->getProductosConsignacion() as $pCv) {
                    if ($pCv->getProducto()->getCodigo() == $codigo) {
                        if ($pCv->getCantidad() > $cantidad) {
                            $resta = $pCv->getCantidad() - $cantidad;
                            $pCv->setCantidad($cantidad);
                            $stk = $pCv->getProducto()->getStock() + $resta;
                            $pCv->getProducto()->setStock($stk);
                            $pCv->setPrecio($request->get('precio' . $x));
                            $em->flush();
                        } else {
                            if ($pCv->getCantidad() < $cantidad) {
                                $resta = $cantidad - $pCv->getCantidad();
                                $pCv->setCantidad($cantidad);
                                $stk = $pCv->getProducto()->getStock() - $resta;
                                if ($stk <= 0) {
                                    $stk = 0;
                                }
                                $pCv->getProducto()->setStock($stk);
                                $pCv->setPrecio($request->get('precio' . $x));
                                $em->flush();
                            } else {
                                $pCv->setPrecio($request->get('precio' . $x));
                            }
                        }
                        break;
                    }
                }
            } else {
                $codigo = $request->get('codigo' . $x);
                $desc = $request->get('descripcion' . $x);
                if (!is_null($codigo) and $codigo != '' and $desc != '' and ! is_null($desc)) {
                    $productoConsignacion = new ProductoConsignacion();

                    if ($codigo != '000') {
                        $producto = $em->getRepository('JOYASJoyasBundle:Producto')->findOneBy(array('codigo' => $codigo, 'unidadNegocio' => $entity->getListaPrecio()->getUnidadNegocio()));
                    } else {
                        $producto = $em->getRepository('JOYASJoyasBundle:Producto')->findOneBy(array('codigo' => $codigo));
                    }

                    if (!is_null($producto) and $codigo != '000') {
                        $stock = $producto->getStock() - $request->get('cantidad' . $x);
                        if ($stock < 0) {
                            $stock = 0;
                        }
                        $producto->setStock($stock);
                    }

                    if (!is_null($producto)) {
                        $productoConsignacion->setProducto($producto);
                        $productoConsignacion->setConsignacion($entity);
                        $productoConsignacion->setPrecio($request->get('precio' . $x));
                        $productoConsignacion->setCantidad($request->get('cantidad' . $x));
                        $entity->addProductosConsignacion($productoConsignacion);
                    }
                }
            }
        }

        if (count($entity->getProductosConsignacion()) < 1) {
            $em->remove($entity);
            $em->flush();
            $this->sessionSvc->addFlash('msgError', 'La consignacion debe tener al menos un producto.');
            $this->sessionSvc->setSession('listaprecio', $listaPrecio->getId());
            return $this->redirect($this->generateUrl('consignacion_new'));
        }
        $em->flush();

        $this->sessionSvc->setSession('listaprecio', '');
        $this->sessionSvc->addFlash('msgOk', 'Consignacion registrada.');

        return $this->redirect($this->generateUrl('consignacion_show', array('id' => $entity->getId())));
    }

    /**
     * Deletes a Consignacion entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JOYASJoyasBundle:Consignacion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Consignacion entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('consignacion'));
    }

    /**
     * Creates a form to delete a Consignacion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('consignacion_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
