<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JOYAS\JoyasBundle\Entity\Cotizacion;
use JOYAS\JoyasBundle\Form\CotizacionType;
use Symfony\Component\HttpFoundation\Session\Session;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Cotizacion controller.
 *
 */
class CotizacionController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionSvc;

    /**
     * Lists all Cotizacion entities.
     *
     */
    public function indexAction() {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
            $unidadNegocio = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($this->sessionSvc->getSession('unidad'));
            if ($unidadNegocio->getUsaParametrica()) {
                $entities = $em->getRepository('JOYASJoyasBundle:Cotizacion')->findBy(array('unidadNegocio' => $unidadNegocio));
            } else {
                $entities = $em->getRepository('JOYASJoyasBundle:Cotizacion')->findBy(array('unidadNegocio' => null));
            }
        } else {
            $entities = $em->getRepository('JOYASJoyasBundle:Cotizacion')->findAll();
        }
        return $this->render('JOYASJoyasBundle:Cotizacion:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new Cotizacion entity.
     *
     */
    public function createAction(Request $request) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $entity = new Cotizacion();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);       
        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->sessionSvc->addFlash("msgOk", "Cotizaciones dadas de alta exitosamente");
            return $this->redirect($this->generateUrl('cotizacion'));
        }

        return $this->render('JOYASJoyasBundle:Cotizacion:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Cotizacion entity.
     *
     * @param Cotizacion $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Cotizacion $entity) {
        $form = $this->createForm(new CotizacionType(), $entity, array(
            'action' => $this->generateUrl('cotizacion_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn middle-first crear')));

        return $form;
    }

    /**
     * Displays a form to create a new Cotizacion entity.
     *
     */
    public function newAction() {
        $entity = new Cotizacion();
        $form = $this->createCreateForm($entity);

        return $this->render('JOYASJoyasBundle:Cotizacion:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Cotizacion entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Cotizacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cotizacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Cotizacion:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Cotizacion entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Cotizacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cotizacion entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Cotizacion:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Cotizacion entity.
     *
     * @param Cotizacion $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Cotizacion $entity) {
        $form = $this->createForm(new CotizacionType(), $entity, array(
            'action' => $this->generateUrl('cotizacion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr' => array('class' => 'btn middle-first')));

        return $form;
    }

    /**
     * Edits an existing Cotizacion entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Cotizacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cotizacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('cotizacion_edit', array('id' => $id)));
        }

        return $this->render('JOYASJoyasBundle:Cotizacion:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Cotizacion entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JOYASJoyasBundle:Cotizacion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Cotizacion entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('cotizacion'));
    }

    /**
     * Creates a form to delete a Cotizacion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('cotizacion_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
