<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JOYAS\JoyasBundle\Entity\Cheque;
use JOYAS\JoyasBundle\Form\ChequeType;
use Symfony\Component\HttpFoundation\Session\Session;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Cheque controller.
 *
 */
class ChequeController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionSvc;

    /**
     * Lists all Cotizacion entities.
     *
     */
    public function indexAction() {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findBy(array('estado' => 'A'));
        if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
            $entities = $em->getRepository('JOYASJoyasBundle:Cheque')->findBy(array('unidadNegocio' => $this->sessionSvc->getSession('unidad')));
        } else {
            $entities = $em->getRepository('JOYASJoyasBundle:Cheque')->findAll();
        }

        return $this->render('JOYASJoyasBundle:Cheque:cheques.html.twig', array(
                    'entities' => $entities,
                    'unidades' => $unidades,
        ));
    }

    /**
     * Creates a new Cheque entity.
     *
     */
    public function createAction(Request $request) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $entity = new Cheque();

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setFechaemision(new \DateTime($request->get('fechaemision')));
            $entity->setFechacobro(new \DateTime($request->get('fechacobro')));
            $entity->setFecha(new \DateTime("now", new \DateTimeZone('America/Argentina/Buenos_Aires')));

            $idUnidadNegocio = $this->sessionSvc->getSession('unidad');
            $unidadNegocio = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($idUnidadNegocio);
            $entity->setUnidadNegocio($unidadNegocio);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('cheque_show', array('id' => $entity->getId())));
        }

        return $this->render('JOYASJoyasBundle:Cheque:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Cheque entity.
     *
     * @param Cheque $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Cheque $entity) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $form = $this->createForm(new ChequeType(), $entity, array(
            'action' => $this->generateUrl('cheque_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn btn-success middle-first crear')));

        return $form;
    }

    /**
     * Displays a form to create a new Cheque entity.
     *
     */
    public function newAction() {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $entity = new Cheque();
        $form = $this->createCreateForm($entity);

        return $this->render('JOYASJoyasBundle:Cheque:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Cheque entity.
     *
     */
    public function showAction($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Cheque')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cheque entity.');
        }

       
        return $this->render('JOYASJoyasBundle:Cheque:show.html.twig', array(
                    'entity' => $entity
        ));
    }

    /**
     * Displays a form to edit an existing Cheque entity.
     *
     */
    public function editAction($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Cheque')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cheque entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Cheque:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Cheque entity.
     *
     * @param Cheque $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Cheque $entity) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $form = $this->createForm(new ChequeType(), $entity, array(
            'action' => $this->generateUrl('cheque_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr' => array('class' => 'btn middle-first')));

        return $form;
    }

    /**
     * Edits an existing Cheque entity.
     *
     */
    public function updateAction(Request $request, $id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Cheque')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cheque entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('cheque_edit', array('id' => $id)));
        }

        return $this->render('JOYASJoyasBundle:Cheque:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Cheque entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JOYASJoyasBundle:Cheque')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Cheque entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('cheque'));
    }

    /**
     * Creates a form to delete a Cheque entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('cheque_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
