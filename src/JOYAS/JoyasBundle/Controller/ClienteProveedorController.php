<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JOYAS\JoyasBundle\Entity\ClienteProveedor;
use JOYAS\JoyasBundle\Form\ClienteProveedorType;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;
/**
 * ClienteProveedor controller.
 *
 */
class ClienteProveedorController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionSvc;

    /**
     * Lists all ClienteProveedor entities.
     *
     */
    public function indexAction(Request $request, $page = 1) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
 
        $unidadActual = $this->sessionSvc->getSession('unidad');
        $unidad = $request->get('unidadNegocio');
        $clienteProveedor = $request->get('clienteProveedor');
        $buscador = $request->get('buscador');

        $this->sessionSvc->setSession('bUnidadNegocio', $unidad);
        $this->sessionSvc->setSession('bClienteProveedor', $clienteProveedor);
        $this->sessionSvc->setSession('bBuscador', $buscador);

        $em = $this->getDoctrine()->getManager();       
        $entities = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->filtroBusqueda($unidad, $clienteProveedor, $buscador, $unidadActual);
        
        $unidades = null;
        if ($this->sessionSvc->getSession('perfil') == 'ADMINISTRADOR') {
            $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        }

        $paginador = new Pagerfanta(new ArrayAdapter($entities));
        $paginador->setMaxPerPage(40);
        $paginador->setCurrentPage($page);

        return $this->render('JOYASJoyasBundle:ClienteProveedor:index.html.twig', array(
                    'entities' => $paginador,
                    'unidades' => $unidades
        ));
     
    }

    /**
     * Creates a new ClienteProveedor entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new ClienteProveedor();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($entity->getClienteProveedor() != '1' and $entity->getClienteProveedor() != '2') {
                $this->sessionSvc->addFlash('msgWarn', 'Por favor vuelva a seleccionar Cliente/Proveedor.');
                return $this->render('JOYASJoyasBundle:ClienteProveedor:new.html.twig', array(
                            'entity' => $entity,
                            'form' => $form->createView(),
                ));
            }

            if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
                $unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($this->sessionSvc->getSession('unidad'));
            } else {
                $unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($request->get('unidadnegocio'));
            }

            $entity->setUnidadNegocio($unidad);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('clienteproveedor_show', array('id' => $entity->getId())));
        }

        return $this->render('JOYASJoyasBundle:ClienteProveedor:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a ClienteProveedor entity.
     *
     * @param ClienteProveedor $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ClienteProveedor $entity) {
        $form = $this->createForm(new ClienteProveedorType(), $entity, array(
            'action' => $this->generateUrl('clienteproveedor_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn middle-first')));

        return $form;
    }

    /**
     * Displays a form to create a new ClienteProveedor entity.
     *
     */
    public function newAction() {
        $entity = new ClienteProveedor();
        $form = $this->createCreateForm($entity);

        $em = $this->getDoctrine()->getManager();
        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        $localidades = $em->getRepository('JOYASJoyasBundle:Localidad')->findBy(array('activo' => true), array('descripcion' => 'ASC'));

        return $this->render('JOYASJoyasBundle:ClienteProveedor:new.html.twig', array(
                    'entity' => $entity,
                    'unidades' => $unidades,
                    'localidades' => $localidades,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ClienteProveedor entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ClienteProveedor entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:ClienteProveedor:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),));
    }

    public function ccAction($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ClienteProveedor entity.');
        }

        $this->sessionSvc->setSession('idCliProv', $id);

        return $this->render('JOYASJoyasBundle:ClienteProveedor:cuentacorriente.html.twig', array(
                    'entity' => $entity,
        ));
    }

    public function imprimirAction($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $em = $this->getDoctrine()->getManager();

        $clienteProveedor = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->find($id);

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator($this->container->getParameter('nombre_cliente'))
                ->setLastModifiedBy($this->container->getParameter('nombre_cliente'))
                ->setTitle("Informe")
                ->setSubject($this->container->getParameter('nombre_cliente'));

        $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Cuenta Corriente: "' . $clienteProveedor->getRazonSocial() . '"')
                ->setCellValue('D1', 'Pesos ($)')
                ->setCellValue('G1', 'Dolar(u$s)')
                ->setCellValue('J1', 'Plata (gr)')
                ->setCellValue('M1', 'Oro (gr)');

        $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A2', 'Fecha')
                ->setCellValue('B2', 'Movimiento')
                ->setCellValue('C2', 'Debe')
                ->setCellValue('D2', 'Haber')
                ->setCellValue('E2', 'Saldo')
                ->setCellValue('F2', 'Debe')
                ->setCellValue('G2', 'Haber')
                ->setCellValue('H2', 'Saldo')
                ->setCellValue('I2', 'Debe')
                ->setCellValue('J2', 'Haber')
                ->setCellValue('K2', 'Saldo')
                ->setCellValue('L2', 'Debe')
                ->setCellValue('M2', 'Haber')
                ->setCellValue('N2', 'Saldo');

        $count = 3;
        $suma = 0;

        $pesosD = 0;
        $pesosH = 0;
        $dolaresD = 0;
        $dolaresH = 0;
        $plataD = 0;
        $plataH = 0;
        $oroD = 0;
        $oroH = 0;

        $tipo = '';

        foreach ($clienteProveedor->getMovimientoscc() as $entity) {
            if ($entity->getTipoDocumento() == 'C') {
                $tipo = 'NOTA DE CREDITO ' . $entity->getId();
            }
            if ($entity->getTipoDocumento() == 'D') {
                $tipo = 'NOTA DE DEBITO ' . $entity->getId();
            }
            if ($entity->getTipoDocumento() == 'FC' or $entity->getTipoDocumento() == 'F') {
                $tipo = 'FACTURA ' . $entity->getId();
            }
            if ($entity->getTipoDocumento() == 'R') {
                $tipo = 'RECIBO CLIENTE ' . $entity->getId();
            }
            if ($entity->getTipoDocumento() == 'RP') {
                $tipo = 'RECIBO PROV ' . $entity->getId();
            }

            if ($entity->getTipoDocumento() == 'FC') {
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $entity->getFactura()->getFecha()->format('d-m-Y'))
                        ->setCellValue('B' . $count, $tipo);
            } else {
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A' . $count, $entity->getDocumento()->getFechaRegistracion()->format('d-m-Y'))
                        ->setCellValue('B' . $count, $tipo);
            }

            if ($entity->getEstado() == 'A' and $entity->getClienteProveedor()->getClienteProveedor() == '2') {
                if ($entity->getTipoDocumento() == 'RP' or $entity->getTipoDocumento() == 'C') {
                    if ($entity->getMonedaStr() == 'ARG') {
                        $pesosD = $entity->getDocumento()->getImporte() + $pesosD;
                        $operacion = $pesosD - $pesosH;
                        $phpExcelObject->setActiveSheetIndex(0)
                                ->setCellValue('C' . $count, '$ ' . $entity->getDocumento()->getImporte())
                                ->setCellValue('E' . $count, '$ ' . round($operacion, 2));
                    } else {
                        $dolaresD = $entity->getDocumento()->getImporte() + $dolaresD;
                        $operacion = $dolaresD - $dolaresH;
                        $phpExcelObject->setActiveSheetIndex(0)
                                ->setCellValue('F' . $count, 'U$S ' . $entity->getDocumento()->getImporte())
                                ->setCellValue('H' . $count, 'U$S ' . round($operacion, 2));
                    }
                    if (null !== $entity->getDocumento()->getPlata()) {
                        $plataD = $entity->getDocumento()->getPlata() + $plataD;
                        $operacion = $plataD - $plataH;
                        $phpExcelObject->setActiveSheetIndex(0)
                                ->setCellValue('I' . $count, $entity->getDocumento()->getPlata() . ' gr')
                                ->setCellValue('K' . $count, round($operacion, 2) . ' gr');
                    }
                    if (null !== $entity->getDocumento()->getOro()) {
                        $oroD = $entity->getDocumento()->getOro() + $oroD;
                        $operacion = $oroD - $oroH;
                        $phpExcelObject->setActiveSheetIndex(0)
                                ->setCellValue('L' . $count, $entity->getDocumento()->getOro() . ' gr')
                                ->setCellValue('N' . $count, round($operacion, 2) . ' gr');
                    }
                }
                if ($entity->getTipoDocumento() == 'F' or $entity->getTipoDocumento() == 'D') {
                    if ($entity->getMonedaStr() == 'ARG') {
                        $pesosH = $entity->getDocumento()->getImporte() + $pesosH;
                        $operacion = $pesosD - $pesosH;
                        $phpExcelObject->setActiveSheetIndex(0)
                                ->setCellValue('D' . $count, '$ ' . $entity->getDocumento()->getImporte())
                                ->setCellValue('E' . $count, '$ ' . round($operacion, 2));
                    } else {
                        $dolaresH = $entity->getDocumento()->getImporte() + $dolaresH;
                        $operacion = $dolaresD - $dolaresH;
                        $phpExcelObject->setActiveSheetIndex(0)
                                ->setCellValue('G' . $count, 'U$S ' . $entity->getDocumento()->getImporte())
                                ->setCellValue('H' . $count, 'U$S ' . round($operacion, 2));
                    }
                    if (null !== $entity->getDocumento()->getPlata()) {
                        $plataH = $entity->getDocumento()->getPlata() + $plataH;
                        $operacion = $plataD - $plataH;
                        $phpExcelObject->setActiveSheetIndex(0)
                                ->setCellValue('I' . $count, $entity->getDocumento()->getPlata() . ' gr')
                                ->setCellValue('K' . $count, round($operacion, 2) . ' gr');
                    }
                    if (null !== $entity->getDocumento()->getOro()) {
                        $oroH = $entity->getDocumento()->getOro() + $oroH;
                        $operacion = $oroD - $oroH;
                        $phpExcelObject->setActiveSheetIndex(0)
                                ->setCellValue('L' . $count, $entity->getDocumento()->getOro() . ' gr')
                                ->setCellValue('N' . $count, round($operacion, 2) . ' gr');
                    }
                }
            } else {
                if ($entity->getEstado() == 'A' and $entity->getClienteProveedor()->getClienteProveedor() == '1') {
                    if ($entity->getTipoDocumento() == 'D' or $entity->getTipoDocumento() == 'FC') {
                        $this->sessionSvc->setSession('segundoIF cliente', 'SI');
                        if ($entity->getMonedaStr() == 'ARG') {
                            if ($entity->getTipoDocumento() == 'D') {
                                $pesosD = $entity->getDocumento()->getImporte() + $pesosD;
                                $operacion = $pesosD - $pesosH;
                                $phpExcelObject->setActiveSheetIndex(0)
                                        ->setCellValue('C' . $count, '$ ' . $entity->getDocumento()->getImporte())
                                        ->setCellValue('E' . $count, '$ ' . round($operacion, 2));
                            } else {
                                $importeFC = 0;
                                foreach ($entity->getFactura()->getProductosFactura() as $prodFact) {
                                    $importeFC = ($prodFact->getPrecio() * $prodFact->getCantidad()) + $importeFC;
                                }

                                if ($entity->getfactura()->getDescuento() != '') {
                                    $desc = '0.' . (100 - $entity->getFactura()->getDescuento());
                                    $importeFC = $importeFC * $desc;
                                }

                                if ($entity->getFactura()->getBonificacion() != '') {
                                    $importeFC = $importeFC - $entity->getFactura()->getBonificacion();
                                }

                                $pesosD = $importeFC + $pesosD;
                                $operacion = $pesosD - $pesosH;
                                $this->sessionSvc->setSession('importefactura' . $entity->getId(), $importeFC);
                                $phpExcelObject->setActiveSheetIndex(0)
                                        ->setCellValue('C' . $count, '$ ' . round($importeFC, 2))
                                        ->setCellValue('E' . $count, '$ ' . round($operacion, 2));
                            }
                        } else {
                            if ($entity->getTipoDocumento() == 'D') {
                                $dolaresD = $entity->getDocumento()->getImporte() + $dolaresD;
                                $operacion = $dolaresD - $dolaresH;
                                $phpExcelObject->setActiveSheetIndex(0)
                                        ->setCellValue('F' . $count, 'U$S ' . $entity->getDocumento()->getImporte())
                                        ->setCellValue('H' . $count, 'U$S ' . round($operacion, 2));
                            } else {
                                $importeFC = 0;
                                foreach ($entity->getFactura()->getProductosFactura() as $prodFact) {
                                    $importeFC = ($prodFact->getPrecio() * $prodFact->getCantidad()) + $importeFC;
                                }

                                if ($entity->getFactura()->getDescuento() != '') {
                                    $desc = '0.' . (100 - $entity->getFactura()->getDescuento());
                                    $importeFC = $importeFC * $desc;
                                }

                                if ($entity->getFactura()->getBonificacion() != '') {
                                    $importeFC = $importeFC - $entity->getFactura()->getBonificacion();
                                }

                                $dolaresD = $importeFC + $dolaresD;
                                $operacion = $dolaresD - $dolaresH;
                                $phpExcelObject->setActiveSheetIndex(0)
                                        ->setCellValue('F' . $count, 'U$S ' . round($importeFC, 2))
                                        ->setCellValue('H' . $count, 'U$S ' . round($operacion, 2));
                            }
                        }
                    if ($entity->getDocumento() && null !== $entity->getDocumento()->getPlata()) {
                        if ($entity->getTipoDocumento() == 'D') {
                                $plataD = $entity->getDocumento()->getPlata() + $plataD;
                                $operacion = $plataD - $plataH;
                                $phpExcelObject->setActiveSheetIndex(0)
                                        ->setCellValue('I' . $count,  $entity->getDocumento()->getPlata().' gr')
                                        ->setCellValue('K' . $count,  round($operacion, 2).' gr');
                            } 
                    }
                    if ($entity->getDocumento() && null !== $entity->getDocumento()->getOro()) {
                        if ($entity->getTipoDocumento() == 'D') {
                                $oroD = $entity->getDocumento()->getOro() + $oroD;
                                $operacion = $oroD - $oroH;
                                $phpExcelObject->setActiveSheetIndex(0)
                                        ->setCellValue('I' . $count,  $entity->getDocumento()->getOro().' gr')
                                        ->setCellValue('K' . $count,  round($operacion, 2).' gr');
                            } 
                    }
                        
                        
                    }
                    if ($entity->getTipoDocumento() == 'R' or $entity->getTipoDocumento() == 'C') {
                        if ($entity->getMonedaStr() == 'ARG') {
                            $pesosH = $entity->getDocumento()->getImporte() + $pesosH;
                            $operacion = $pesosD - $pesosH;
                            $phpExcelObject->setActiveSheetIndex(0)
                                    ->setCellValue('D' . $count, '$ ' . $entity->getDocumento()->getImporte())
                                    ->setCellValue('E' . $count, '$ ' . round($operacion, 2));
                        } else {
                            $dolaresH = $entity->getDocumento()->getImporte() + $dolaresH;
                            $operacion = $dolaresD - $dolaresH;
                            $phpExcelObject->setActiveSheetIndex(0)
                                    ->setCellValue('G' . $count, 'U$S ' . $entity->getDocumento()->getImporte())
                                    ->setCellValue('H' . $count, 'U$S ' . round($operacion, 2));
                        }
                        if ($entity->getDocumento() && null !== $entity->getDocumento()->getPlata()) {
                            $plataH = $entity->getDocumento()->getPlata() + $plataH;
                            $operacion = $plataD - $plataH;
                            $phpExcelObject->setActiveSheetIndex(0)
                                    ->setCellValue('J' . $count,  $entity->getDocumento()->getPlata().' gr')
                                    ->setCellValue('K' . $count, 'U$S ' . round($operacion, 2).' gr');
                        }
                        if ($entity->getDocumento() && null !== $entity->getDocumento()->getOro()) {
                            $oroH = $entity->getDocumento()->getOro() + $oroH;
                            $operacion = $oroD - $oroH;
                            $phpExcelObject->setActiveSheetIndex(0)
                                    ->setCellValue('M' . $count,  $entity->getDocumento()->getOro().' gr')
                                    ->setCellValue('N' . $count, 'U$S ' . round($operacion, 2).' gr');
                        }                        
                    }
                }
            }
            $count++;
        }

        $sumaPesos = $pesosD - $pesosH;
        $sumaDolares = $dolaresD - $dolaresH;
        $sumaPlata = $plataD - $plataH;
        $sumaOro = $oroD - $oroH;

        $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('D' . $count, 'SALDO CAJA $ ' . round($sumaPesos, 2))
                ->setCellValue('G' . $count, 'SALDO CAJA U$S ' . round($sumaDolares, 2))
                ->setCellValue('J' . $count, 'SALDO CAJA Plata ' . round($sumaPlata, 2))
                ->setCellValue('M' . $count, 'SALDO CAJA Oro ' . round($sumaOro, 2));


        $phpExcelObject->getActiveSheet()->getStyle('A2:N2')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');

        $phpExcelObject->getActiveSheet()->setTitle('Cuenta Corriente');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $phpExcelObject->setActiveSheetIndex(0);
        $phpExcelObject->getActiveSheet()->getStyle('D' . $count)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');
        $phpExcelObject->getActiveSheet()->getStyle('G' . $count)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');
        $phpExcelObject->getActiveSheet()->getStyle('J' . $count)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');
        $phpExcelObject->getActiveSheet()->getStyle('M' . $count)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');
        $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('G')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('H')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('I')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('J')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('K')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('L')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('M')->setWidth(25);
        $phpExcelObject->getActiveSheet()->getColumnDimension('N')->setWidth(25);

        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename=CuentaCorriente-' . $clienteProveedor->getRazonSocial() . '.xls');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');

        return $response;
    }

    /**
     * Displays a form to edit an existing ClienteProveedor entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->find($id);
        $localidades = $em->getRepository('JOYASJoyasBundle:Localidad')->findBy(array('activo' => true), array('descripcion' => 'ASC'));
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ClienteProveedor entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('JOYASJoyasBundle:ClienteProveedor:edit.html.twig', array(
                    'entity' => $entity,
                    'localidades' => $localidades,
                    'edit_form' => $editForm->createView()
        ));
    }

    /**
     * Creates a form to edit a ClienteProveedor entity.
     *
     * @param ClienteProveedor $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(ClienteProveedor $entity) {
        $form = $this->createForm(new ClienteProveedorType(), $entity, array(
            'action' => $this->generateUrl('clienteproveedor_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr' => array('class' => 'btn middle-first')));

        return $form;
    }

    /**
     * Edits an existing ClienteProveedor entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ClienteProveedor entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
//			$entity->setClienteProveedor($request->get('clienteproveedor'));
            $em->flush();

            return $this->redirect($this->generateUrl('clienteproveedor'));
        }

        return $this->render('JOYASJoyasBundle:ClienteProveedor:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a ClienteProveedor entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ClienteProveedor entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('clienteproveedor'));
    }

    /**
     * Creates a form to delete a ClienteProveedor entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('clienteproveedor_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    public function filtroAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        if ($request->get('razonsocial') != '') {
            if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
                $entities = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->filtro($request->get('razonsocial'), $this->sessionSvc->getSession('unidad'));
            } else {
                $entities = $em->getRepository('JOYASJoyasBundle:ClienteProveedor')->filtro($request->get('razonsocial'));
            }

            return $this->render('JOYASJoyasBundle:ClienteProveedor:index.html.twig', array(
                        'entities' => $entities,
            ));
        } else {
            return $this->redirect($this->generateUrl('clienteproveedor'));
        }
    }

}
