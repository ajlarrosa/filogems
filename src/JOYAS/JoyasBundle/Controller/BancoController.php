<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\ArrayCollection;
use JOYAS\JoyasBundle\Entity\Banco;
use JOYAS\JoyasBundle\Form\BancoType;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Banco controller.
 *
 */
class BancoController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionSvc;

    /**
     * Lists all Banco entities.
     *
     */
    public function indexAction() {
        if ($this->sessionSvc->isLogged()) {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JOYASJoyasBundle:Banco')->findAll();

        return $this->render('JOYASJoyasBundle:Banco:index.html.twig', array(
                    'entities' => $entities,
        ));
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
    }
    }

    /**
     * Creates a new Banco entity.
     *
     */
    public function createAction(Request $request) {
        if ($this->sessionSvc->isLogged()) {
        $entity = new Banco();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
                $this->sessionSvc->addFlash('msgOk', 'Banco creado exitosamente!');
                return $this->redirect($this->generateUrl('banco', array('id' => $entity->getId())));
        }

        return $this->render('JOYASJoyasBundle:Banco:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
    }
    }

    /**
     * Creates a form to create a Banco entity.
     *
     * @param Banco $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Banco $entity) {
        $form = $this->createForm(new BancoType(), $entity, array(
            'action' => $this->generateUrl('banco_create'),
            'method' => 'POST',
        ));
        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn middle-first')));
        return $form;
    }

    /**
     * Displays a form to create a new Banco entity.
     *
     */
    public function newAction() {
        if ($this->sessionSvc->isLogged()) {
        $entity = new Banco();
        $form = $this->createCreateForm($entity);

        return $this->render('JOYASJoyasBundle:Banco:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
    }
    }

    /**
     * Finds and displays a Banco entity.
     *
     */
    public function showAction($id) {
        if ($this->sessionSvc->isLogged()) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Banco')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Banco entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Banco:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
    }
    }

    /**
     * Displays a form to edit an existing Banco entity.
     *
     */
    public function editAction($id) {
        if ($this->sessionSvc->isLogged()) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Banco')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Banco entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Banco:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
        } else {
            return $this->redirect($this->generateUrl('alerodriguez_homepage'));
    }
    }

    /**
     * Creates a form to edit a Banco entity.
     *
     * @param Banco $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Banco $entity) {
        $form = $this->createForm(new BancoType(), $entity, array(
            'action' => $this->generateUrl('banco_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));
        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn middle-first')));


        return $form;
    }

    /**
     * Edits an existing Banco entity.
     *
     */
    public function updateAction(Request $request, $id) {
        if ($this->sessionSvc->isLogged()) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Banco')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Banco entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
                $this->sessionSvc->addFlash('msgOk', 'Edición realizada correctamente!');
            return $this->redirect($this->generateUrl('banco_edit', array('id' => $id)));
        }

        return $this->render('JOYASJoyasBundle:Banco:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
    }
    }

    /**
     * Deletes a Banco entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JOYASJoyasBundle:Banco')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Banco entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('banco'));
    }

    /**
     * Creates a form to delete a Banco entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('banco_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
