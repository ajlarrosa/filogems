<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JOYAS\JoyasBundle\Entity\ListaPrecio;
use JOYAS\JoyasBundle\Form\ListaPrecioType;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * ListaPrecio controller.
 *
 */
class ListaPrecioController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionSvc;

    /**
     * Lists all ListaPrecio entities.
     *
     */
    public function indexAction() {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
            $entities = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->getAllActivas($this->sessionSvc->getSession('unidad'));
        } else {
            $entities = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->getAllActivas();
        }

        return $this->render('JOYASJoyasBundle:ListaPrecio:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new ListaPrecio entity.
     *
     */
    public function createAction(Request $request) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $entity = new ListaPrecio();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
                $unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($this->sessionSvc->getSession('unidad'));
            } else {
                $unidad = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->find($request->get('unidadnegocio'));
            }

            $entity->setUnidadNegocio($unidad);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('listaprecio'));
        }

        return $this->render('JOYASJoyasBundle:ListaPrecio:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a ListaPrecio entity.
     *
     * @param ListaPrecio $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ListaPrecio $entity) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $form = $this->createForm(new ListaPrecioType(), $entity, array(
            'action' => $this->generateUrl('listaprecio_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn middle-first crear')));

        return $form;
    }

    /**
     * Displays a form to create a new ListaPrecio entity.
     *
     */
    public function newAction() {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $em = $this->getDoctrine()->getManager();
        $unidades = $em->getRepository('JOYASJoyasBundle:UnidadNegocio')->findAll();
        $entity = new ListaPrecio();
        $form = $this->createCreateForm($entity);

        return $this->render('JOYASJoyasBundle:ListaPrecio:new.html.twig', array(
                    'entity' => $entity,
                    'unidades' => $unidades,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ListaPrecio entity.
     *
     */
    public function showAction($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ListaPrecio entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:ListaPrecio:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),));
    }

    /**
     * Displays a form to edit an existing ListaPrecio entity.
     *
     */
    public function editAction($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ListaPrecio entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:ListaPrecio:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a ListaPrecio entity.
     *
     * @param ListaPrecio $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(ListaPrecio $entity) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $form = $this->createForm(new ListaPrecioType(), $entity, array(
            'action' => $this->generateUrl('listaprecio_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr' => array('class' => 'btn middle-first')));
        return $form;
    }

    /**
     * Edits an existing ListaPrecio entity.
     *
     */
    public function updateAction(Request $request, $id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ListaPrecio entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('listaprecio'));
        }

        return $this->render('JOYASJoyasBundle:ListaPrecio:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a ListaPrecio entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ListaPrecio entity.');
        }
        $entity->setEstado('I');
        $em->persist($entity);
        $em->flush();
        $this->sessionSvc->addFlash('msgWarn', 'Lista de Precios eliminado correctamente.');
        return $this->redirect($this->generateUrl('listaprecio'));
    }

    /**
     * Creates a form to delete a ListaPrecio entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('listaprecio_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    /**
     *
     */
    public function informePreciosAction() {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
            $listas = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->getAllActivas($this->sessionSvc->getSession('unidad'));
        } else {
            $listas = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->getAllActivas();
        }
        return $this->render('JOYASJoyasBundle:ListaPrecio:filtro.html.twig', array(
                    'listas' => $listas,
        ));
    }

    public function actualizacionMasivaAction() {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        if ($this->sessionSvc->getSession('perfil') != 'ADMINISTRADOR') {
            $listas = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->getAllActivas($this->sessionSvc->getSession('unidad'));
        } else {
            $listas = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->getAllActivas();
        }
        return $this->render('JOYASJoyasBundle:ListaPrecio:actualizacionmasiva.html.twig', array(
                    'listas' => $listas,
        ));
    }

    /**
     *
     */
    public function generarInformePreciosAction(Request $request) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $lista = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->find($request->get('listado'));

        // ask the service for a Excel5
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator($this->container->getParameter('nombre_cliente'))
                ->setLastModifiedBy($this->container->getParameter('nombre_cliente'))
                ->setTitle("Informe")
                ->setSubject($this->container->getParameter('nombre_cliente'));

        $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Fecha ' . date('d-m-Y'))
                ->setCellValue('A2', 'Lista de Precios: "' . $lista->getDescripcion() . '"')
                ->setCellValue('B2', 'Moneda "' . $lista->getMonedaStr() . '"');

        $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A3', 'CODIGO')
                ->setCellValue('B3', 'DESCRIPCION')
                ->setCellValue('C3', 'PRECIO')
        ;

        $count = 4;

        foreach ($lista->getPrecios() as $precio) {

            $phpExcelObject->setActiveSheetIndex(0)
                    ->setCellValue('A' . $count, $precio->getProducto()->getCodigo())
                    ->setCellValue('B' . $count, $precio->getProducto()->getDescripcion())
                    ->setCellValue('C' . $count, $lista->getMonedaStr() . ' ' . $precio->getValor());

            $count = $count + 1;
        }


        $phpExcelObject->getActiveSheet()->getStyle('A3:C3')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('B2B2B2B2');


        $phpExcelObject->getActiveSheet()->setTitle('Cuentas por Cobrar');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $phpExcelObject->setActiveSheetIndex(0);
        $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $phpExcelObject->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $phpExcelObject->getActiveSheet()->getColumnDimension('C')->setWidth(30);

        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename=InformePrecios.xls');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');

        return $response;
    }

    public function listaParaImportacionAction(Request $request) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $lista = $em->getRepository('JOYASJoyasBundle:ListaPrecio')->find($request->get('lista'));

        // ask the service for a Excel5
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator($this->container->getParameter('nombre_cliente'))
                ->setLastModifiedBy($this->container->getParameter('nombre_cliente'))
                ->setTitle("Informe de Stocks y Precios para actualización")
                ->setSubject($this->container->getParameter('nombre_cliente'));

        $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Categoria Subcategoria')
                ->setCellValue('B1', 'Codigo')
                ->setCellValue('C1', 'Cantidad')
                ->setCellValue('D1', 'Hechura')
                ->setCellValue('E1', 'Moneda')
                ->setCellValue('F1', 'Peso Unitario AG')
                ->setCellValue('G1', 'Peso Unitario AU')
                ->setCellValue('H1', 'Peso Unitario Quilates')
                ->setCellValue('I1', 'Descripcion')
                ->setCellValue('J1', 'Tipo de Stock')
                ->setCellValue('K1', $lista->getDescripcion())
        ;

        $count = 2;
        foreach ($lista->getPrecios() as $precio) {
            if ($precio->getProducto()->getEstado() == 'A' && $precio->getEstado()=='A') {
                if ($precio->getProducto()->getCategoriasubcategoria()) {
                    $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue('A' . $count, $precio->getProducto()->getCategoriasubcategoria()->getId());
                } else {
                    $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue('A' . $count, '');
                }
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('B' . $count, $precio->getProducto()->getCodigo())
                        ->setCellValue('C' . $count, $precio->getProducto()->getStock())
                        ->setCellValue('D' . $count, $precio->getProducto()->getCosto())
                        ->setCellValue('E' . $count, $precio->getProducto()->getMonedaStr())
                        ->setCellValue('F' . $count, $precio->getProducto()->getPlata())
                        ->setCellValue('G' . $count, $precio->getProducto()->getOro())
                        ->setCellValue('H' . $count, $precio->getProducto()->getQuilate())
                        ->setCellValue('I' . $count, $precio->getProducto()->getDescripcion())
                        ->setCellValue('J' . $count, $precio->getProducto()->getTipoStock())
                        ->setCellValue('K' . $count, $precio->getValor())
                ;
                $count++;
            }
        }
        $phpExcelObject->setActiveSheetIndex(0);
        $phpExcelObject->getActiveSheet()->setTitle('Hoja1');
        $phpExcelObject->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('K')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('L')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('M')->setWidth(20);
        $phpExcelObject->getActiveSheet()->getColumnDimension('N')->setWidth(20);


        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename=ListaParaActualizacion.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');

        return $response;
    }

}
