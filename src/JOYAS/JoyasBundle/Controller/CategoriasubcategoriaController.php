<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JOYAS\JoyasBundle\Entity\Categoriasubcategoria;
use JOYAS\JoyasBundle\Form\CategoriasubcategoriaType;
use Symfony\Component\HttpFoundation\Session\Session;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Categoriasubcategoria controller.
 *
 */
class CategoriasubcategoriaController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionSvc;

    /**
     * Lists all Categoriasubcategoria entities.
     *
     */
    public function indexAction() {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JOYASJoyasBundle:Categoriasubcategoria')->findBy(array('estado'=>'A'));

        return $this->render('JOYASJoyasBundle:Categoriasubcategoria:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new Categoriasubcategoria entity.
     *
     */
    public function createAction(Request $request) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $entity = new Categoriasubcategoria();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $catsub = $em->getRepository('JOYASJoyasBundle:Categoriasubcategoria')->findOneBy(array('categoria' => $entity->getCategoria()->getId(), 'subcategoria' => $entity->getSubcategoria()->getId()));
            if (is_null($catsub)) {
                $em->persist($entity);
                $em->flush();
                return $this->redirect($this->generateUrl('categoriasubcategoria'));
            } else {
                $this->sessionSvc->addFlash('msgWarn', 'No puede haber dos Categorias - Subcategorias iguales.');
            }
        }

        return $this->render('JOYASJoyasBundle:Categoriasubcategoria:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Categoriasubcategoria entity.
     *
     * @param Categoriasubcategoria $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Categoriasubcategoria $entity) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $form = $this->createForm(new CategoriasubcategoriaType(), $entity, array(
            'action' => $this->generateUrl('categoriasubcategoria_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn middle-first crear')));

        return $form;
    }

    /**
     * Displays a form to create a new Categoriasubcategoria entity.
     *
     */
    public function newAction() {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $entity = new Categoriasubcategoria();
        $form = $this->createCreateForm($entity);

        return $this->render('JOYASJoyasBundle:Categoriasubcategoria:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Categoriasubcategoria entity.
     *
     */
    public function showAction($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Categoriasubcategoria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Categoriasubcategoria entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Categoriasubcategoria:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Categoriasubcategoria entity.
     *
     */
    public function editAction($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Categoriasubcategoria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Categoriasubcategoria entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Categoriasubcategoria:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Categoriasubcategoria entity.
     *
     * @param Categoriasubcategoria $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Categoriasubcategoria $entity) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $form = $this->createForm(new CategoriasubcategoriaType(), $entity, array(
            'action' => $this->generateUrl('categoriasubcategoria_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr' => array('class' => 'btn middle-first')));

        return $form;
    }

    /**
     * Edits an existing Categoriasubcategoria entity.
     *
     */
    public function updateAction(Request $request, $id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Categoriasubcategoria')->find($id);

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $catsub = $em->getRepository('JOYASJoyasBundle:Categoriasubcategoria')->findOneBy(array('categoria' => $entity->getCategoria()->getId(), 'subcategoria' => $entity->getSubcategoria()->getId()));
            if (is_null($catsub)) {
                $em->flush();
                return $this->redirect($this->generateUrl('categoriasubcategoria'));
            } else {
                $this->sessionSvc->addFlash('msgWarn', 'No puede haber dos Categorias - Subcategorias iguales.');
            }
        }

        return $this->render('JOYASJoyasBundle:Categoriasubcategoria:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a Categoriasubcategoria entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Categoriasubcategoria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Categoriasubcategoria entity.');
        }
        $entity->setEstado('I');
        $em->persist($entity);
        $em->flush();
        $this->sessionSvc->addFlash('msgWarn', 'Relación entre categoría y subcategoría eliminada correctamente.');
        return $this->redirect($this->generateUrl('categoriasubcategoria'));
    }

    /**
     * Creates a form to delete a Categoriasubcategoria entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }

        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('categoriasubcategoria_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
