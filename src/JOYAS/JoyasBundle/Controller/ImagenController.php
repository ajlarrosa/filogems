<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\ArrayCollection;
use JOYAS\JoyasBundle\Entity\Imagen;
use JOYAS\JoyasBundle\Form\ImagenType;
use Symfony\Component\HttpFoundation\Session\Session;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Imagen controller.
 *
 */
class ImagenController extends Controller {

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionSvc;

    /**
     * Lists all Imagen entities.
     *
     */
    public function indexAction() {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JOYASJoyasBundle:Imagen')->findAll();

        return $this->render('JOYASJoyasBundle:Imagen:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new Imagen entity.
     *
     */
    public function createAction(Request $request) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $entity = new Imagen();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);


        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if (!empty($_FILES['joyas_joyasbundle_imagen']['tmp_name']['foto'])) {
                $content = "data:" . $_FILES['joyas_joyasbundle_imagen']['type']['foto'] . ";base64," . base64_encode(file_get_contents($_FILES['joyas_joyasbundle_imagen']['tmp_name']['foto']));
                $entity->setFoto($content);
                $originalName = $_FILES['joyas_joyasbundle_imagen']['name']['foto'];
                $entity->setPath($originalName);
            }
            $em->persist($entity);
            $em->flush();
            $this->sessionSvc->addFlash('msgOk', 'Se creó una nueva imagen para la portada.');
            return $this->redirect($this->generateUrl('imagen_show', array('id' => $entity->getId())));
        }
        $this->sessionSvc->addFlash('msgError', 'No se ha podido cargar la imagen.');
        return $this->render('JOYASJoyasBundle:Imagen:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Imagen entity.
     *
     * @param Imagen $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Imagen $entity) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $form = $this->createForm(new ImagenType(), $entity, array(
            'action' => $this->generateUrl('imagen_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Imagen entity.
     *
     */
    public function newAction() {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $entity = new Imagen();
        $form = $this->createCreateForm($entity);

        return $this->render('JOYASJoyasBundle:Imagen:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Imagen entity.
     *
     */
    public function showAction($id) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Imagen')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Socio entity.');
        }

        return $this->render('JOYASJoyasBundle:Imagen:show.html.twig', array(
                    'entity' => $entity
        ));
    }

    /**
     * Edits an existing Imagen entity.
     *
     */
    public function updateAction($id, $estado) {
        if (!$this->sessionSvc->isLogged()) {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Imagen')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Imagen entity.');
        }

        $entity->setEstado($estado);
        $em->persist($entity);

        $em->flush();
        $this->sessionSvc->addFlash('msgOk', 'Se editó el estado de la imagen.');
        return $this->redirect($this->generateUrl('imagen'));
    }

}
