<?php

namespace JOYAS\JoyasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\ArrayCollection;
use JOYAS\JoyasBundle\Entity\Provincia;
use JOYAS\JoyasBundle\Form\ProvinciaType;
use JOYAS\JoyasBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Provincia controller.
 *
 */
class ProvinciaController extends Controller
{

    /**
     * @var SessionManager
     * @DI\Inject("session.manager")
     */
    public $sessionSvc;

    /**
     * Lists all Provincia entities.
     *
     */
    public function indexAction() {
        if ($this->sessionSvc->isLogged()) {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JOYASJoyasBundle:Provincia')->findAll();

        return $this->render('JOYASJoyasBundle:Provincia:index.html.twig', array(
            'entities' => $entities,
                        'title' => 'Provincias'
        ));
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
    }
    }

    /**
     * Creates a new Provincia entity.
     *
     */
    public function createAction(Request $request) {
        if ($this->sessionSvc->isLogged()) {
            $entity = new Provincia();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
                $this->sessionSvc->addFlash('msgOk', 'Provincia creada exitosamente!');
                return $this->redirect($this->generateUrl('provincia', array('id' => $entity->getId())));
        }

        return $this->render('JOYASJoyasBundle:Provincia:new.html.twig', array(
            'entity' => $entity,
                        'form' => $form->createView(),
                        'title' =>'Alta de Provincia'
        ));
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
    }
    }

    /**
     * Creates a form to create a Provincia entity.
     *
     * @param Provincia $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Provincia $entity)
    {
        $form = $this->createForm(new ProvinciaType(), $entity, array(
            'action' => $this->generateUrl('provincia_create'),
            'method' => 'POST',
        ));
        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn middle-first')));
        return $form;
    }

    /**
     * Displays a form to create a new Provincia entity.
     *
     */
    public function newAction() {
        if ($this->sessionSvc->isLogged()) {
            $entity = new Provincia();
            $form = $this->createCreateForm($entity);

        return $this->render('JOYASJoyasBundle:Provincia:new.html.twig', array(
            'entity' => $entity,
                        'form' => $form->createView(),
                        'title' => 'Alta de Provincia'
        ));
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
    }
    }

    /**
     * Finds and displays a Provincia entity.
     *
     */
    public function showAction($id) {
        if ($this->sessionSvc->isLogged()) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Provincia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Provincia entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Provincia:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
    }
    }

    /**
     * Displays a form to edit an existing Provincia entity.
     *
     */
    public function editAction($id) {
        if ($this->sessionSvc->isLogged()) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Provincia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Provincia entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('JOYASJoyasBundle:Provincia:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
                        'title' => 'Edición de Provincia'
        ));
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
    }
    }

    /**
    * Creates a form to edit a Provincia entity.
    *
    * @param Provincia $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Provincia $entity)
    {
        $form = $this->createForm(new ProvinciaType(), $entity, array(
            'action' => $this->generateUrl('provincia_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));
        $form->add('submit', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn middle-first')));


        return $form;
    }

    /**
     * Edits an existing Provincia entity.
     *
     */
    public function updateAction(Request $request, $id) {
        if ($this->sessionSvc->isLogged()) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JOYASJoyasBundle:Provincia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Provincia entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
                $this->sessionSvc->addFlash('msgOk', 'Edición realizada correctamente!');
                return $this->redirect($this->generateUrl('provincia_edit', array('id' => $id)));
        }

        return $this->render('JOYASJoyasBundle:Provincia:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
                        'title' => 'Edición de Provincia'
        ));
        } else {
            return $this->redirect($this->generateUrl('joyas_joyas_homepage'));
    }
    }

    /**
     * Deletes a Provincia entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JOYASJoyasBundle:Provincia')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Provincia entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('provincia'));
    }

    /**
     * Creates a form to delete a Provincia entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('provincia_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
