<?php

namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\ProvinciaRepository")
 * @ORM\Table(name="provincia")
 * @UniqueEntity(
 * 		fields = {"descripcion"},
 * 			message = "Ya esxiste esta provincia."
 * 		) 
 */
class Provincia {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255,nullable=false)
     * @Assert\Length(
     *      max = 255,     
     *      maxMessage = "La descripción no puede superar los 255 caracteres"
     * )   
     */
    protected $descripcion;

    /**
     * @ORM\OneToMany(targetEntity="Localidad", mappedBy="provincia")
     */
    protected $localidades;
    
    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default"=1})
     */
    protected $activo = true;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->localidades = new ArrayCollection();         
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Provincia
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return Provincia
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    
        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Add localidades
     *
     * @param \JOYAS\JoyasBundle\Entity\Localidad $localidades
     * @return Provincia
     */
    public function addLocalidade(\JOYAS\JoyasBundle\Entity\Localidad $localidades)
    {
        $this->localidades[] = $localidades;
    
        return $this;
    }

    /**
     * Remove localidades
     *
     * @param \JOYAS\JoyasBundle\Entity\Localidad $localidades
     */
    public function removeLocalidade(\JOYAS\JoyasBundle\Entity\Localidad $localidades)
    {
        $this->localidades->removeElement($localidades);
    }

    /**
     * Get localidades
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLocalidades()
    {
        return $this->localidades;
    }
}