<?php

namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\TipoGastoRepository")
 * @ORM\Table(name="tipogasto")
 */
class TipoGasto {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
     /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="tiposGastos")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id")
     */
    protected $unidadNegocio;

    /**
     * @ORM\Column(type="string", length=300)
     */
    protected $nombre;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**
     * @ORM\OneToMany(targetEntity="Documento", mappedBy="tipoGasto")
     */
    protected $documentos;
    
    

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->documentos = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getNombre();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return TipoGasto
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return TipoGasto
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }


    /**
     * Add documentos
     *
     * @param \JOYAS\JoyasBundle\Entity\Documento $documentos
     * @return TipoGasto
     */
    public function addDocumento(\JOYAS\JoyasBundle\Entity\Documento $documentos)
    {
        $this->documentos[] = $documentos;
    
        return $this;
    }

    /**
     * Remove documentos
     *
     * @param \JOYAS\JoyasBundle\Entity\Documento $documentos
     */
    public function removeDocumento(\JOYAS\JoyasBundle\Entity\Documento $documentos)
    {
        $this->documentos->removeElement($documentos);
    }

    /**
     * Get documentos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDocumentos()
    {
        return $this->documentos;
    }

    /**
     * Set unidadNegocio
     *
     * @param \JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio
     * @return TipoGasto
     */
    public function setUnidadNegocio(\JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio = null)
    {
        $this->unidadNegocio = $unidadNegocio;
    
        return $this;
    }

    /**
     * Get unidadNegocio
     *
     * @return \JOYAS\JoyasBundle\Entity\UnidadNegocio 
     */
    public function getUnidadNegocio()
    {
        return $this->unidadNegocio;
    }
}