<?php

namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\MovimientoChequeRepository")
 * @ORM\Table(name="movimientocheque")
 */
class MovimientoCheque {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Cheque")
     * @ORM\JoinColumn(name="cheque_id", referencedColumnName="id", nullable=false)
     */
    protected $cheque;

    /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio")
     * @ORM\JoinColumn(name="unidadNegocio_id", referencedColumnName="id", nullable=true)
     */
    protected $unidadNegocio;

    /**
     * @ORM\Column(type="string")
     */
    protected $tipoMovimiento;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechaMovimiento;

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        
    }

    public function __toString() {
        if ($this->tipoMovimiento == 'MANUAL') {
            return 'Ingreso Manual';
        }
        if ($this->tipoMovimiento == 'PAGO') {
            return 'RECIBO PROV. ' . $this->cheque->getPago()->getId();
        }
        return 'RECIBO CLIENTE ' . $this->cheque->getDocumento()->getId();
    }

    /**
     * Set tipoMovimiento
     *
     * @param string $tipoMovimiento
     * @return MovimientoCheque
     */
    public function setTipoMovimiento($tipoMovimiento) {
        $this->tipoMovimiento = $tipoMovimiento;

        return $this;
    }

    /**
     * Get tipoMovimiento
     *
     * @return string 
     */
    public function getTipoMovimiento() {
        return $this->tipoMovimiento;
    }

    /**
     * Set fechaMovimiento
     *
     * @param \DateTime $fechaMovimiento
     * @return MovimientoCheque
     */
    public function setFechaMovimiento($fechaMovimiento) {
        $this->fechaMovimiento = $fechaMovimiento;

        return $this;
    }

    /**
     * Get fechaMovimiento
     *
     * @return \DateTime 
     */
    public function getFechaMovimiento() {
        return $this->fechaMovimiento;
    }

    /**
     * Set cheque
     *
     * @param \JOYAS\JoyasBundle\Entity\Cheque $cheque
     * @return MovimientoCheque
     */
    public function setCheque(\JOYAS\JoyasBundle\Entity\Cheque $cheque) {
        $this->cheque = $cheque;

        return $this;
    }

    /**
     * Get cheque
     *
     * @return \JOYAS\JoyasBundle\Entity\Cheque 
     */
    public function getCheque() {
        return $this->cheque;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set unidadNegocio
     *
     * @param \JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio
     * @return MovimientoCheque
     */
    public function setUnidadNegocio(\JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio = null)
    {
        $this->unidadNegocio = $unidadNegocio;
    
        return $this;
    }

    /**
     * Get unidadNegocio
     *
     * @return \JOYAS\JoyasBundle\Entity\UnidadNegocio 
     */
    public function getUnidadNegocio()
    {
        return $this->unidadNegocio;
    }
}