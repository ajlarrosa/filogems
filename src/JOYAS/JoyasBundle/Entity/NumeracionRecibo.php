<?php

namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\NumeracionReciboRepository")
 * @ORM\Table(name="numeracionrecibo")
 */
class NumeracionRecibo {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="numeracionrecibo")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id", nullable=true)
     */
    protected $unidadNegocio;

    /**
     * @ORM\Column(type="integer")
     */
    protected $nrorecibo;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nrorecibo
     *
     * @param integer $nrorecibo
     * @return NumeracionRecibo
     */
    public function setNrorecibo($nrorecibo) {
        $this->nrorecibo = $nrorecibo;

        return $this;
    }

    /**
     * Get nrorecibo
     *
     * @return integer 
     */
    public function getNrorecibo() {
        return $this->nrorecibo;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return NumeracionRecibo
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set unidadNegocio
     *
     * @param \JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio
     * @return NumeracionRecibo
     */
    public function setUnidadNegocio(\JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio = null) {
        $this->unidadNegocio = $unidadNegocio;

        return $this;
    }

    /**
     * Get unidadNegocio
     *
     * @return \JOYAS\JoyasBundle\Entity\UnidadNegocio 
     */
    public function getUnidadNegocio() {
        return $this->unidadNegocio;
    }

}
