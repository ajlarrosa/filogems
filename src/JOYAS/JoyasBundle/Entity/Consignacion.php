<?php

namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\ConsignacionRepository")
 * @ORM\Table(name="consignacion")
 */
class Consignacion {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechaVencimiento;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $descuento;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $bonificacion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $importe;

    /**
     * @ORM\OneToMany(targetEntity="ProductoConsignacion", mappedBy="consignacion", cascade={"persist"})
     */
    protected $productosConsignacion;

    /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="consignaciones")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id")
     */
    protected $unidadNegocio;

    /**
     * @ORM\ManyToOne(targetEntity="ClienteProveedor", inversedBy="consignaciones")
     * @ORM\JoinColumn(name="clienteproveedor_id", referencedColumnName="id")
     */
    protected $clienteProveedor;

    /**
     * @ORM\ManyToOne(targetEntity="ListaPrecio")
     * @ORM\JoinColumn(name="listaprecio_id", referencedColumnName="id")
     */
    protected $listaPrecio;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->productosConsignacion = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Consignacion
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set fechaVencimiento
     *
     * @param \DateTime $fechaVencimiento
     * @return Consignacion
     */
    public function setFechaVencimiento($fechaVencimiento) {
        $this->fechaVencimiento = $fechaVencimiento;

        return $this;
    }

    /**
     * Get fechaVencimiento
     *
     * @return \DateTime 
     */
    public function getFechaVencimiento() {
        return $this->fechaVencimiento;
    }

    /**
     * Set descuento
     *
     * @param float $descuento
     * @return Consignacion
     */
    public function setDescuento($descuento) {
        $this->descuento = $descuento;

        return $this;
    }

    /**
     * Get descuento
     *
     * @return float 
     */
    public function getDescuento() {
        return $this->descuento;
    }

    /**
     * Set importe
     *
     * @param float $importe
     * @return Consignacion
     */
    public function setImporte($importe) {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return float 
     */
    public function getImporte() {
        return $this->importe;
    }

    /**
     * Set bonificacion
     *
     * @param float $bonificacion
     * @return Consignacion
     */
    public function setBonificacion($bonificacion) {
        $this->bonificacion = $bonificacion;

        return $this;
    }

    /**
     * Get bonificacion
     *
     * @return float 
     */
    public function getBonificacion() {
        return $this->bonificacion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Consignacion
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Add productosConsignacion
     *
     * @param \JOYAS\JoyasBundle\Entity\ProductoConsignacion $productosConsignacion
     * @return Consignacion
     */
    public function addProductosConsignacion(\JOYAS\JoyasBundle\Entity\ProductoConsignacion $productosConsignacion) {
        $this->productosConsignacion[] = $productosConsignacion;

        return $this;
    }

    /**
     * Remove productosConsignacion
     *
     * @param \JOYAS\JoyasBundle\Entity\ProductoConsignacion $productosConsignacion
     */
    public function removeProductosConsignacion(\JOYAS\JoyasBundle\Entity\ProductoConsignacion $productosConsignacion) {
        $this->productosConsignacion->removeElement($productosConsignacion);
    }

    /**
     * Get productosConsignacion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductosConsignacion() {
        return $this->productosConsignacion;
    }

    /**
     * Set unidadNegocio
     *
     * @param \JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio
     * @return Consignacion
     */
    public function setUnidadNegocio(\JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio = null) {
        $this->unidadNegocio = $unidadNegocio;

        return $this;
    }

    /**
     * Get unidadNegocio
     *
     * @return \JOYAS\JoyasBundle\Entity\UnidadNegocio 
     */
    public function getUnidadNegocio() {
        return $this->unidadNegocio;
    }

    /**
     * Set listaPrecio
     *
     * @param \JOYAS\JoyasBundle\Entity\ListaPrecio $listaPrecio
     * @return Consignacion
     */
    public function setListaPrecio(\JOYAS\JoyasBundle\Entity\ListaPrecio $listaPrecio = null) {
        $this->listaPrecio = $listaPrecio;

        return $this;
    }

    /**
     * Get listaPrecio
     *
     * @return \JOYAS\JoyasBundle\Entity\ListaPrecio 
     */
    public function getListaPrecio() {
        return $this->listaPrecio;
    }

    /**
     * Set clienteProveedor
     *
     * @param \JOYAS\JoyasBundle\Entity\ClienteProveedor $clienteProveedor
     * @return Consignacion
     */
    public function setClienteProveedor(\JOYAS\JoyasBundle\Entity\ClienteProveedor $clienteProveedor = null) {
        $this->clienteProveedor = $clienteProveedor;

        return $this;
    }

    /**
     * Get clienteProveedor
     *
     * @return \JOYAS\JoyasBundle\Entity\ClienteProveedor 
     */
    public function getClienteProveedor() {
        return $this->clienteProveedor;
    }

}
