<?php

namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\MovimientoCCRepository")
 * @ORM\Table(name="movimientocc")
 */
class MovimientoCC {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="Factura")
     * @ORM\JoinColumn(name="factura_id", referencedColumnName="id", nullable=true)
     * */
    private $factura;

    /**
     * @ORM\OneToOne(targetEntity="Documento")
     * @ORM\JoinColumn(name="documento_id", referencedColumnName="id", nullable=true)
     * */
    private $documento;

    /**
     * @ORM\ManyToOne(targetEntity="ClienteProveedor", inversedBy="movimientoscc")
     * @ORM\JoinColumn(name="clienteProveedor_id", referencedColumnName="id", nullable=true)
     */
    protected $clienteProveedor;

    /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="movimientoscc")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id", nullable=true)
     */
    protected $unidadNegocio;

    /**
     * @ORM\Column(type="string", length=2, nullable=false)
     */
    protected $tipoDocumento;

    /**
     * @ORM\Column(type="string", length=3, nullable=false)
     */
    protected $moneda;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $tarjeta;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getNombre();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set uid
     *
     * @param string $uid
     * @return MovimientoCC
     */
    public function setUid($uid) {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get uid
     *
     * @return string 
     */
    public function getUid() {
        return $this->uid;
    }

    /**
     * Set fechaRegistracion
     *
     * @param \DateTime $fechaRegistracion
     * @return MovimientoCC
     */
    public function setFechaRegistracion($fechaRegistracion) {
        $this->fechaRegistracion = $fechaRegistracion;

        return $this;
    }

    /**
     * Get fechaRegistracion
     *
     * @return \DateTime 
     */
    public function getFechaRegistracion() {
        return $this->fechaRegistracion;
    }

    /**
     * Set tipoDocumento
     *
     * @param string $tipoDocumento
     * @return MovimientoCC
     */
    public function setTipoDocumento($tipoDocumento) {
        $this->tipoDocumento = $tipoDocumento;

        return $this;
    }

    /**
     * Get tipoDocumento
     *
     * @return string 
     */
    public function getTipoDocumento() {
        return $this->tipoDocumento;
    }

    /**
     * Set moneda
     *
     * @param string $moneda
     * @return MovimientoCC
     */
    public function setMoneda($moneda) {
        $this->moneda = $moneda;

        return $this;
    }

    /**
     * Get moneda
     *
     * @return string 
     */
    public function getMoneda() {
        return $this->moneda;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return MovimientoCC
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Set clienteProveedor
     *
     * @param \JOYAS\JoyasBundle\Entity\ClienteProveedor $clienteProveedor
     * @return MovimientoCC
     */
    public function setClienteProveedor(\JOYAS\JoyasBundle\Entity\ClienteProveedor $clienteProveedor) {
        $this->clienteProveedor = $clienteProveedor;

        return $this;
    }

    /**
     * Get clienteProveedor
     *
     * @return \JOYAS\JoyasBundle\Entity\ClienteProveedor 
     */
    public function getClienteProveedor() {
        return $this->clienteProveedor;
    }

    /**
     * Get moneda
     *
     * @return string 
     */
    public function getMonedaStr() {
        if ($this->moneda == 1) {
            return 'ARG';
        }
        if ($this->moneda == 2) {
            return 'USD';
        }
    }

    public function getMonedaSimbolo() {
        if ($this->moneda == 1) {
            return '$';
        }
        if ($this->moneda == 2) {
            return 'u$s';
        }
    }

    /**
     * Set factura
     *
     * @param \JOYAS\JoyasBundle\Entity\Factura $factura
     * @return MovimientoCC
     */
    public function setFactura(\JOYAS\JoyasBundle\Entity\Factura $factura = null) {
        $this->factura = $factura;

        return $this;
    }

    /**
     * Get factura
     *
     * @return \JOYAS\JoyasBundle\Entity\Factura 
     */
    public function getFactura() {
        return $this->factura;
    }

    /**
     * Set documento
     *
     * @param \JOYAS\JoyasBundle\Entity\Documento $documento
     * @return MovimientoCC
     */
    public function setDocumento(\JOYAS\JoyasBundle\Entity\Documento $documento = null) {
        $this->documento = $documento;

        return $this;
    }

    /**
     * Get documento
     *
     * @return \JOYAS\JoyasBundle\Entity\Documento 
     */
    public function getDocumento() {
        return $this->documento;
    }

    /**
     * Set unidadNegocio
     *
     * @param \JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio
     * @return MovimientoCC
     */
    public function setUnidadNegocio(\JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio = null) {
        $this->unidadNegocio = $unidadNegocio;

        return $this;
    }

    /**
     * Get unidadNegocio
     *
     * @return \JOYAS\JoyasBundle\Entity\UnidadNegocio 
     */
    public function getUnidadNegocio() {
        return $this->unidadNegocio;
    }

    /**
     * Set tarjeta
     *
     * @param string $tarjeta
     * @return MovimientoCC
     */
    public function setTarjeta($tarjeta) {
        $this->tarjeta = $tarjeta;
        return $this;
    }

    /**
     * Get tarjeta
     *
     * @return string 
     */
    public function getTarjeta() {
        return $this->tarjeta;
    }

}
