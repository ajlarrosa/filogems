<?php

namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\LocalidadRepository")
 * @ORM\Table(name="localidad")
 * @UniqueEntity(
 * 		fields = {"descripcion","provincia"},
 * 			message = "Ya esxiste esta localidad para esta provincia."
 * 		) 
 */
class Localidad {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Provincia", inversedBy="localidades")
     * @ORM\JoinColumn(name="provincia_id", referencedColumnName="id")
     */
    protected $provincia;

    /**
     * @ORM\Column(type="string", length=255,nullable=false)
     * @Assert\Length(
     *      max = 255,     
     *      maxMessage = "La descripción no puede superar los 255 caracteres"
     * )   
     * @ORM\OrderBy({"descripcion" = "ASC"})     * 
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $cp;    
    
    /**
     * @ORM\OneToMany(targetEntity="ClienteProveedor", mappedBy="localidad")
     */
    protected $clientesproveedores;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default"=1})
     */
    protected $activo = true;

    /*********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->localidadesSucursal = new ArrayCollection();
        $this->clientesproveedores = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Localidad
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return Localidad
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    
        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set provincia
     *
     * @param \JOYAS\JoyasBundle\Entity\Provincia $provincia
     * @return Localidad
     */
    public function setProvincia(\JOYAS\JoyasBundle\Entity\Provincia $provincia = null)
    {
        $this->provincia = $provincia;
    
        return $this;
    }

    /**
     * Get provincia
     *
     * @return \JOYAS\JoyasBundle\Entity\Provincia 
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Add clientesproveedores
     *
     * @param \JOYAS\JoyasBundle\Entity\ClienteProveedor $clientesproveedores
     * @return Localidad
     */
    public function addLocalidadesClientesProveedore(\JOYAS\JoyasBundle\Entity\ClienteProveedor $clientesproveedores)
    {
        $this->clientesproveedores[] = $clientesproveedores;
    
        return $this;
    }

    /**
     * Remove clientesproveedores
     *
     * @param \JOYAS\JoyasBundle\Entity\ClienteProveedor $clientesproveedores
     */
    public function removeLocalidadesClientesProveedore(\JOYAS\JoyasBundle\Entity\ClienteProveedor $clientesproveedores)
    {
        $this->clientesproveedores->removeElement($clientesproveedores);
    }

    /**
     * Get clientesproveedores
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLocalidadesClientesProveedores()
    {
        return $this->clientesproveedores;
    }


    /**
     * Set cp
     *
     * @param integer $cp
     * @return Localidad
     */
    public function setCp($cp)
    {
        $this->cp = $cp;
    
        return $this;
    }

    /**
     * Get cp
     *
     * @return integer 
     */
    public function getCp()
    {
        return $this->cp;
    }
}