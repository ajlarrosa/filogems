<?php

namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Table(name="producto")
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\ProductoRepository")
 */
class Producto {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)    
     */
    protected $descripcionampliada;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(
     *       message = "Debe agregar una descripción."
     *              )
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank(
     *       message = "Debe indicar el stock."
     *              )
     */
    protected $stock;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $plata = 0;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $oro = 0;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $quilate = 0;

    /**
     * @ORM\Column(type="string", length=3)
     */
    protected $moneda;

    /**
     * @ORM\Column(type="float", nullable=false)
     * @Assert\NotBlank(
     *       message = "Debe agregar un costo."
     *              )
     */
    protected $costo;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $codigo;
    
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $referencia;

    /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="productos")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id")
     */
    protected $unidadNegocio;

    /**
     * @ORM\ManyToOne(targetEntity="Categoriasubcategoria", inversedBy="productos")
     * @ORM\JoinColumn(name="categoriasubcategoria_id", referencedColumnName="id")     
     */
    protected $categoriasubcategoria;

    /**
     * @ORM\OneToMany(targetEntity="Precio", mappedBy="producto", cascade={"persist", "remove"})
     */
    protected $precios;

    /**
     * @ORM\OneToMany(targetEntity="ProductoFactura", mappedBy="producto")
     */
    protected $productosFactura;

    /**
     * @ORM\OneToMany(targetEntity="ImagenProducto", mappedBy="producto", cascade={"persist", "remove"})
     */
    protected $imagenesproducto;

    /**
     * @ORM\OneToMany(targetEntity="ProductoDocumento", mappedBy="producto", cascade={"remove"})
     */
    protected $productosDocumento;

    /**
     * @ORM\OneToMany(targetEntity="ProductoConsignacion", mappedBy="producto", cascade={"remove"})
     */
    protected $productosConsignacion;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $tipoStock = 'U'; //Valores Posibles U O P

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Assert\NotNull(message = "El campo no puede estar en blanco.")
     * @Assert\Choice(choices = {true, false}, message = "Elija un valor.")
     */
    protected $visible = true;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Assert\Choice(choices = {true, false}, message = "Elija un valor.")
     */
    protected $novedad = false;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Assert\Choice(choices = {true, false}, message = "Elija un valor.")
     */
    protected $destacado = false;

    /**
     * @ORM\OneToMany(targetEntity="DetallePedido", mappedBy="producto")
     */
    protected $detallesPedido;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $descuento = 0;

    
    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->precios = new ArrayCollection();
        $this->productosFactura = new ArrayCollection();
        $this->imagenesproducto = new ArrayCollection();
        $this->productosDocumento = new ArrayCollection();
        $this->productosConsignacion = new ArrayCollection();
        $this->detallesPedido = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion() . ' - ' . $this->getCodigo();
    }

    /**
     * Get moneda
     *
     * @return string 
     */
    public function getMonedaStr() {
        if ($this->moneda == 1) {
            return 'ARG';
        }
        if ($this->moneda == 2) {
            return 'USD';
        }
    }

    public function getMonedaSimbolo() {
        if ($this->moneda == 1) {
            return '$';
        }
        if ($this->moneda == 2) {
            return 'u$s';
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Producto
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set stock
     *
     * @param float $stock
     * @return Producto
     */
    public function setStock($stock) {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return float 
     */
    public function getStock() {
        return $this->stock;
    }

    /**
     * Set plata
     *
     * @param float $plata
     * @return Producto
     */
    public function setPlata($plata) {
        $this->plata = $plata;

        return $this;
    }

    /**
     * Get plata
     *
     * @return float 
     */
    public function getPlata() {
        return $this->plata;
    }

    /**
     * Set oro
     *
     * @param float $oro
     * @return Producto
     */
    public function setOro($oro) {
        $this->oro = $oro;

        return $this;
    }

    /**
     * Get oro
     *
     * @return float 
     */
    public function getOro() {
        return $this->oro;
    }

    /**
     * Set moneda
     *
     * @param string $moneda
     * @return Producto
     */
    public function setMoneda($moneda) {
        $this->moneda = $moneda;

        return $this;
    }

    /**
     * Get moneda
     *
     * @return string 
     */
    public function getMoneda() {
        return $this->moneda;
    }

    /**
     * Set costo
     *
     * @param float $costo
     * @return Producto
     */
    public function setCosto($costo) {
        $this->costo = $costo;

        return $this;
    }

    /**
     * Get costo
     *
     * @return float 
     */
    public function getCosto() {
        return $this->costo;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     * @return Producto
     */
    public function setCodigo($codigo) {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string 
     */
    public function getCodigo() {
        return $this->codigo;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Producto
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Add precios
     *
     * @param \JOYAS\JoyasBundle\Entity\Precio $precios
     * @return Producto
     */
    public function addPrecio(\JOYAS\JoyasBundle\Entity\Precio $precios) {
        $this->precios[] = $precios;

        return $this;
    }

    /**
     * Remove precios
     *
     * @param \JOYAS\JoyasBundle\Entity\Precio $precios
     */
    public function removePrecio(\JOYAS\JoyasBundle\Entity\Precio $precios) {
        $this->precios->removeElement($precios);
    }

    /**
     * Get precios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPrecios() {
        return $this->precios;
    }

    /**
     * Add productosFactura
     *
     * @param \JOYAS\JoyasBundle\Entity\ProductoFactura $productosFactura
     * @return Producto
     */
    public function addProductosFactura(\JOYAS\JoyasBundle\Entity\ProductoFactura $productosFactura) {
        $this->productosFactura[] = $productosFactura;

        return $this;
    }

    /**
     * Remove productosFactura
     *
     * @param \JOYAS\JoyasBundle\Entity\ProductoFactura $productosFactura
     */
    public function removeProductosFactura(\JOYAS\JoyasBundle\Entity\ProductoFactura $productosFactura) {
        $this->productosFactura->removeElement($productosFactura);
    }

    /**
     * Get productosFactura
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductosFactura() {
        return $this->productosFactura;
    }

    /**
     * Add imagenesproducto
     *
     * @param \JOYAS\JoyasBundle\Entity\ImagenProducto $imagenesproducto
     * @return Producto
     */
    public function addImagenesproducto(\JOYAS\JoyasBundle\Entity\ImagenProducto $imagenesproducto) {
        $this->imagenesproducto[] = $imagenesproducto;

        return $this;
    }

    /**
     * Remove imagenesproducto
     *
     * @param \JOYAS\JoyasBundle\Entity\ImagenProducto $imagenesproducto
     */
    public function removeImagenesproducto(\JOYAS\JoyasBundle\Entity\ImagenProducto $imagenesproducto) {
        $this->imagenesproducto->removeElement($imagenesproducto);
    }

    /**
     * Get imagenesproducto
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImagenesproducto() {
        return $this->imagenesproducto;
    }

    /**
     * Set unidadNegocio
     *
     * @param \JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio
     * @return Producto
     */
    public function setUnidadNegocio(\JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio = null) {
        $this->unidadNegocio = $unidadNegocio;

        return $this;
    }

    /**
     * Get unidadNegocio
     *
     * @return \JOYAS\JoyasBundle\Entity\UnidadNegocio 
     */
    public function getUnidadNegocio() {
        return $this->unidadNegocio;
    }

    /**
     * Set categoriasubcategoria
     *
     * @param \JOYAS\JoyasBundle\Entity\Categoriasubcategoria $categoriasubcategoria
     * @return Producto
     */
    public function setCategoriasubcategoria(\JOYAS\JoyasBundle\Entity\Categoriasubcategoria $categoriasubcategoria = null) {
        $this->categoriasubcategoria = $categoriasubcategoria;

        return $this;
    }

    /**
     * Get categoriasubcategoria
     *
     * @return \JOYAS\JoyasBundle\Entity\Categoriasubcategoria 
     */
    public function getCategoriasubcategoria() {
        return $this->categoriasubcategoria;
    }

    /**
     * Add productosDocumento
     *
     * @param \JOYAS\JoyasBundle\Entity\ProductoDocumento $productosDocumento
     * @return Producto
     */
    public function addProductosDocumento(\JOYAS\JoyasBundle\Entity\ProductoDocumento $productosDocumento) {
        $this->productosDocumento[] = $productosDocumento;

        return $this;
    }

    /**
     * Remove productosDocumento
     *
     * @param \JOYAS\JoyasBundle\Entity\ProductoDocumento $productosDocumento
     */
    public function removeProductosDocumento(\JOYAS\JoyasBundle\Entity\ProductoDocumento $productosDocumento) {
        $this->productosDocumento->removeElement($productosDocumento);
    }

    /**
     * Get productosDocumento
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductosDocumento() {
        return $this->productosDocumento;
    }

    /**
     * Set tipoStock
     *
     * @param string $tipoStock
     * @return Producto
     */
    public function setTipoStock($tipoStock) {
        $this->tipoStock = $tipoStock;

        return $this;
    }

    /**
     * Get tipoStock
     *
     * @return string 
     */
    public function getTipoStock() {
        return $this->tipoStock;
    }

    public function getTipoStockString() {
        switch ($this->getTipoStock()) {
            case 'U':
                return 'Unidades';
            case 'P':
                return 'Gramos de Plata';
            case 'O':
                return 'Gramos de Oro';
            default:
                return '';
        }
    }

    /**
     * Add productosConsignacion
     *
     * @param \JOYAS\JoyasBundle\Entity\ProductoConsignacion $productosConsignacion
     * @return Producto
     */
    public function addProductosConsignacion(\JOYAS\JoyasBundle\Entity\ProductoConsignacion $productosConsignacion) {
        $this->productosConsignacion[] = $productosConsignacion;

        return $this;
    }

    /**
     * Remove productosConsignacion
     *
     * @param \JOYAS\JoyasBundle\Entity\ProductoConsignacion $productosConsignacion
     */
    public function removeProductosConsignacion(\JOYAS\JoyasBundle\Entity\ProductoConsignacion $productosConsignacion) {
        $this->productosConsignacion->removeElement($productosConsignacion);
    }

    /**
     * Get productosConsignacion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductosConsignacion() {
        return $this->productosConsignacion;
    }

    /**
     * Set quilate
     *
     * @param float $quilate
     * @return Producto
     */
    public function setQuilate($quilate) {
        $this->quilate = $quilate;

        return $this;
    }

    /**
     * Get quilate
     *
     * @return float 
     */
    public function getQuilate() {
        return $this->quilate;
    }

    /**
     * Set descripcionampliada
     *
     * @param string $descripcionampliada
     * @return Producto
     */
    public function setDescripcionampliada($descripcionampliada) {
        $this->descripcionampliada = $descripcionampliada;

        return $this;
    }

    /**
     * Get descripcionampliada
     *
     * @return string 
     */
    public function getDescripcionampliada() {
        return $this->descripcionampliada;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * @return Producto
     */
    public function setVisible($visible) {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean 
     */
    public function getVisible() {
        return $this->visible;
    }

    /**
     * Set novedad
     *
     * @param boolean $novedad
     * @return Producto
     */
    public function setNovedad($novedad) {
        $this->novedad = $novedad;

        return $this;
    }

    /**
     * Get novedad
     *
     * @return boolean 
     */
    public function getNovedad() {
        return $this->novedad;
    }

    /**
     * Set destacado
     *
     * @param boolean $destacado
     * @return Producto
     */
    public function setDestacado($destacado) {
        $this->destacado = $destacado;

        return $this;
    }

    /**
     * Get destacado
     *
     * @return boolean 
     */
    public function getDestacado() {
        return $this->destacado;
    }

    /**
     * Add detallesPedido
     *
     * @param \JOYAS\JoyasBundle\Entity\DetallePedido $detallesPedido
     * @return Producto
     */
    public function addDetallesPedido(\JOYAS\JoyasBundle\Entity\DetallePedido $detallesPedido) {
        $this->detallesPedido[] = $detallesPedido;

        return $this;
    }

    /**
     * Remove detallesPedido
     *
     * @param \JOYAS\JoyasBundle\Entity\DetallePedido $detallesPedido
     */
    public function removeDetallesPedido(\JOYAS\JoyasBundle\Entity\DetallePedido $detallesPedido) {
        $this->detallesPedido->removeElement($detallesPedido);
    }

    /**
     * Get detallesPedido
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDetallesPedido() {
        return $this->detallesPedido;
    }


    /**
     * Set descuento
     *
     * @param integer $descuento
     * @return Producto
     */
    public function setDescuento($descuento)
    {
        $this->descuento = $descuento;
    
        return $this;
    }

    /**
     * Get descuento
     *
     * @return integer 
     */
    public function getDescuento()
    {
        return $this->descuento;
    }

    /**
     * Set referencia
     *
     * @param string $referencia
     * @return Producto
     */
    public function setReferencia($referencia)
    {
        $this->referencia = $referencia;
    
        return $this;
    }

    /**
     * Get referencia
     *
     * @return string 
     */
    public function getReferencia()
    {
        return $this->referencia;
    }
}