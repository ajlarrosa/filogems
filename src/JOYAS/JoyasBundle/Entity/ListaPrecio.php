<?php

namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JOYAS\JoyasBundle\Entity\TipoCosto;
use JOYAS\JoyasBundle\Entity\Ganancia;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\ListaPrecioRepository")
 * @ORM\Table(name="listaprecio")
 */
class ListaPrecio {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="string", length=3, nullable=false)
     */
    protected $moneda;

    /**
     * @ORM\OneToMany(targetEntity="Precio", mappedBy="listaPrecio")
     */
    protected $precios;

    /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="listasprecio")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id")
     */
    protected $unidadNegocio;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->unidadNegocio.' '.$this->getDescripcion() . ' - ' . $this->getMonedaStr();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return ListaPrecio
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set moneda
     *
     * @param string $moneda
     * @return ListaPrecio
     */
    public function setMoneda($moneda) {
        $this->moneda = $moneda;

        return $this;
    }

    /**
     * Get moneda
     *
     * @return string 
     */
    public function getMoneda() {
        return $this->moneda;
    }

    /**
     * Get moneda
     *
     * @return string 
     */
    public function getMonedaStr() {
        if ($this->moneda == 1) {
            return 'ARG';
        }
        if ($this->moneda == 2) {
            return 'USD';
        }
    }
     public function getMonedaSimbolo() {
        if ($this->moneda == 1) {
            return '$';
        }
        if ($this->moneda == 2) {
            return 'u$s';
        }
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return ListaPrecio
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Add precios
     *
     * @param \JOYAS\JoyasBundle\Entity\Precio $precios
     * @return ListaPrecio
     */
    public function addPrecio(\JOYAS\JoyasBundle\Entity\Precio $precios) {
        $this->precios[] = $precios;

        return $this;
    }

    /**
     * Remove precios
     *
     * @param \JOYAS\JoyasBundle\Entity\Precio $precios
     */
    public function removePrecio(\JOYAS\JoyasBundle\Entity\Precio $precios) {
        $this->precios->removeElement($precios);
    }

    /**
     * Get precios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPrecios() {
        return $this->precios;
    }

    /**
     * Set unidadNegocio
     *
     * @param \JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio
     * @return ListaPrecio
     */
    public function setUnidadNegocio(\JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio = null) {
        $this->unidadNegocio = $unidadNegocio;

        return $this;
    }

    /**
     * Get unidadNegocio
     *
     * @return \JOYAS\JoyasBundle\Entity\UnidadNegocio 
     */
    public function getUnidadNegocio() {
        return $this->unidadNegocio;
    }

}
