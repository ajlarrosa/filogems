<?php

namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\ClienteProveedorRepository")
 * @ORM\Table(name="clienteproveedor")
 */
class ClienteProveedor {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $razonSocial;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $direccion;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $telefono;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $celular;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $mail;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $clienteProveedor = '1';

    /**
     * @ORM\OneToMany(targetEntity="MovimientoCC", mappedBy="clienteProveedor")
     */
    protected $movimientoscc;

    /**
     * @ORM\OneToMany(targetEntity="Consignacion", mappedBy="clienteProveedor")
     */
    protected $consignaciones;

    /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="clientesproveedores")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id")
     */
    protected $unidadNegocio;

    /**
     * @ORM\ManyToOne(targetEntity="Localidad", inversedBy="clientesproveedores")
     * @ORM\JoinColumn(name="localidad_id", referencedColumnName="id")
     */
    protected $localidad;
    /**
     * @ORM\Column(type="string", length=30)
     */
    protected $cuit;

    /**
     * @ORM\Column(type="string", length=1)    
     */
    protected $estado = 'A';

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->movimientoscc = new ArrayCollection();
        $this->consignaciones = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getRazonSocial();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set razonSocial
     *
     * @param string $razonSocial
     * @return ClienteProveedor
     */
    public function setRazonSocial($razonSocial) {
        $this->razonSocial = $razonSocial;

        return $this;
    }

    /**
     * Get razonSocial
     *
     * @return string 
     */
    public function getRazonSocial() {
        return $this->razonSocial;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return ClienteProveedor
     */
    public function setDireccion($direccion) {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion() {
        return $this->direccion;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return ClienteProveedor
     */
    public function setTelefono($telefono) {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono() {
        return $this->telefono;
    }

    /**
     * Set celular
     *
     * @param string $celular
     * @return ClienteProveedor
     */
    public function setCelular($celular) {
        $this->celular = $celular;

        return $this;
    }

    /**
     * Get celular
     *
     * @return string 
     */
    public function getCelular() {
        return $this->celular;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return ClienteProveedor
     */
    public function setMail($mail) {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail() {
        return $this->mail;
    }

    /**
     * Set clienteProveedor
     *
     * @param string $clienteProveedor
     * @return ClienteProveedor
     */
    public function setClienteProveedor($clienteProveedor) {
        $this->clienteProveedor = $clienteProveedor;

        return $this;
    }

    /**
     * Get clienteProveedor
     *
     * @return string 
     */
    public function getClienteProveedor() {
        return $this->clienteProveedor;
    }

    /**
     * Get clienteProveedor
     *
     * @return string 
     */
    public function getClienteProveedorStr() {
        if ($this->clienteProveedor == 1) {
            return 'Cliente';
        }
        if ($this->clienteProveedor == 2) {
            return 'Proveedor';
        }
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return ClienteProveedor
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Add movimientoscc
     *
     * @param \JOYAS\JoyasBundle\Entity\MovimientoCC $movimientoscc
     * @return ClienteProveedor
     */
    public function addMovimientoscc(\JOYAS\JoyasBundle\Entity\MovimientoCC $movimientoscc) {
        $this->movimientoscc[] = $movimientoscc;

        return $this;
    }

    /**
     * Remove movimientoscc
     *
     * @param \JOYAS\JoyasBundle\Entity\MovimientoCC $movimientoscc
     */
    public function removeMovimientoscc(\JOYAS\JoyasBundle\Entity\MovimientoCC $movimientoscc) {
        $this->movimientoscc->removeElement($movimientoscc);
    }

    /**
     * Get movimientoscc
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMovimientoscc() {
        return $this->movimientoscc;
    }

    /**
     * Set unidadNegocio
     *
     * @param \JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio
     * @return ClienteProveedor
     */
    public function setUnidadNegocio(\JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio = null) {
        $this->unidadNegocio = $unidadNegocio;

        return $this;
    }

    /**
     * Get unidadNegocio
     *
     * @return \JOYAS\JoyasBundle\Entity\UnidadNegocio 
     */
    public function getUnidadNegocio() {
        return $this->unidadNegocio;
    }

    /**
     * Add consignaciones
     *
     * @param \JOYAS\JoyasBundle\Entity\Consignacion $consignaciones
     * @return ClienteProveedor
     */
    public function addConsignacione(\JOYAS\JoyasBundle\Entity\Consignacion $consignaciones) {
        $this->consignaciones[] = $consignaciones;

        return $this;
    }

    /**
     * Remove consignaciones
     *
     * @param \JOYAS\JoyasBundle\Entity\Consignacion $consignaciones
     */
    public function removeConsignacione(\JOYAS\JoyasBundle\Entity\Consignacion $consignaciones) {
        $this->consignaciones->removeElement($consignaciones);
    }

    /**
     * Get consignaciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConsignaciones() {
        return $this->consignaciones;
    }


    /**
     * Set localidad
     *
     * @param \JOYAS\JoyasBundle\Entity\Localidad $localidad
     * @return ClienteProveedor
     */
    public function setLocalidad(\JOYAS\JoyasBundle\Entity\Localidad $localidad = null)
    {
        $this->localidad = $localidad;
    
        return $this;
    }

    /**
     * Get localidad
     *
     * @return \JOYAS\JoyasBundle\Entity\Localidad 
     */
    public function getLocalidad()
    {
        return $this->localidad;
    }
}