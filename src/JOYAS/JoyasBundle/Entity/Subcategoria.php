<?php

namespace JOYAS\JoyasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JOYAS\JoyasBundle\Entity\TipoCosto;
use JOYAS\JoyasBundle\Entity\Ganancia;

/**
 * @ORM\Entity(repositoryClass="JOYAS\JoyasBundle\Entity\SubcategoriaRepository")
 * @ORM\Table(name="subcategoria")
 */
class Subcategoria {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $descripcion;

    /**
     * @ORM\OneToMany(targetEntity="Categoriasubcategoria", mappedBy="subcategoria")
     */
    protected $categoriasubcategorias;

    /**
     * @ORM\ManyToOne(targetEntity="UnidadNegocio", inversedBy="subcategorias")
     * @ORM\JoinColumn(name="unidadnegocio_id", referencedColumnName="id")
     */
    protected $unidadNegocio;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $sinonimo;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /*     * ********************************
     * __construct
     *
     * 
     * ******************************** */

    public function __construct() {
        $this->categoriasubcategorias = new ArrayCollection();
    }

    /*     * ********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     * ******************************* */

    public function __toString() {
        return $this->getDescripcion();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return ListaPrecio
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Subcategoria
     */
    public function setEstado($estado) {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado() {
        return $this->estado;
    }

    /**
     * Add categoriasubcategorias
     *
     * @param \JOYAS\JoyasBundle\Entity\Categoriasubcategoria $categoriasubcategorias
     * @return Subcategoria
     */
    public function addCategoriasubcategoria(\JOYAS\JoyasBundle\Entity\Categoriasubcategoria $categoriasubcategorias) {
        $this->categoriasubcategorias[] = $categoriasubcategorias;

        return $this;
    }

    /**
     * Remove categoriasubcategorias
     *
     * @param \JOYAS\JoyasBundle\Entity\Categoriasubcategoria $categoriasubcategorias
     */
    public function removeCategoriasubcategoria(\JOYAS\JoyasBundle\Entity\Categoriasubcategoria $categoriasubcategorias) {
        $this->categoriasubcategorias->removeElement($categoriasubcategorias);
    }

    /**
     * Get categoriasubcategorias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategoriasubcategorias() {
        return $this->categoriasubcategorias;
    }

    /**
     * Set unidadNegocio
     *
     * @param \JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio
     * @return Subcategoria
     */
    public function setUnidadNegocio(\JOYAS\JoyasBundle\Entity\UnidadNegocio $unidadNegocio = null) {
        $this->unidadNegocio = $unidadNegocio;

        return $this;
    }

    /**
     * Get unidadNegocio
     *
     * @return \JOYAS\JoyasBundle\Entity\UnidadNegocio 
     */
    public function getUnidadNegocio() {
        return $this->unidadNegocio;
    }


    /**
     * Set sinonimo
     *
     * @param string $sinonimo
     * @return Subcategoria
     */
    public function setSinonimo($sinonimo)
    {
        $this->sinonimo = $sinonimo;
    
        return $this;
    }

    /**
     * Get sinonimo
     *
     * @return string 
     */
    public function getSinonimo()
    {
        return $this->sinonimo;
    }
}